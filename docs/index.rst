.. include:: ../README.rst

Contents
--------

.. toctree::
   :maxdepth: 8

   introduction
   installation
   tutorial
   customization
   modules
   gallery
   contributing
   parallelization
   credits
   history

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
