spux.executors package
======================

.. automodule:: spux.executors
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    spux.executors.balancers
    spux.executors.mpi4py

Submodules
----------

spux.executors.executor module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.executor
    :members:
    :undoc-members:
    :show-inheritance:

spux.executors.serial module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.serial
    :members:
    :undoc-members:
    :show-inheritance:


