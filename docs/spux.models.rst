spux.models package
===================

.. automodule:: spux.models
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.models.ibm module
~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.models.ibm
    :members:
    :undoc-members:
    :show-inheritance:

spux.models.model module
~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.models.model
    :members:
    :undoc-members:
    :show-inheritance:

spux.models.randomwalk module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.models.randomwalk
    :members:
    :undoc-members:
    :show-inheritance:

spux.models.randomwalk\_numba module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.models.randomwalk_numba
    :members:
    :undoc-members:
    :show-inheritance:

spux.models.straightwalk module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.models.straightwalk
    :members:
    :undoc-members:
    :show-inheritance:


