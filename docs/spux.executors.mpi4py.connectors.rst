spux.executors.mpi4py.connectors package
========================================

.. automodule:: spux.executors.mpi4py.connectors
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.executors.mpi4py.connectors.legacy module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.mpi4py.connectors.legacy
    :members:
    :undoc-members:
    :show-inheritance:

spux.executors.mpi4py.connectors.spawn module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.mpi4py.connectors.spawn
    :members:
    :undoc-members:
    :show-inheritance:

spux.executors.mpi4py.connectors.split module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.mpi4py.connectors.split
    :members:
    :undoc-members:
    :show-inheritance:

spux.executors.mpi4py.connectors.utils module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.mpi4py.connectors.utils
    :members:
    :undoc-members:
    :show-inheritance:

spux.executors.mpi4py.connectors.worker module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.executors.mpi4py.connectors.worker
    :members:
    :undoc-members:
    :show-inheritance:


