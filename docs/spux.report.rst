spux.report package
===================

.. automodule:: spux.report
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.report.generate module
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.report.generate
    :members:
    :undoc-members:
    :show-inheritance:


