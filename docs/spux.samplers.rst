spux.samplers package
=====================

.. automodule:: spux.samplers
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.samplers.emcee module
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.samplers.emcee
    :members:
    :undoc-members:
    :show-inheritance:

spux.samplers.forecast module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.samplers.forecast
    :members:
    :undoc-members:
    :show-inheritance:

spux.samplers.mcmc module
~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.samplers.mcmc
    :members:
    :undoc-members:
    :show-inheritance:

spux.samplers.sampler module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.samplers.sampler
    :members:
    :undoc-members:
    :show-inheritance:


