spux.plot package
=================

.. automodule:: spux.plot
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.plot.mpl module
~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.plot.mpl
    :members:
    :undoc-members:
    :show-inheritance:

spux.plot.mpl\_palette\_pf module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.plot.mpl_palette_pf
    :members:
    :undoc-members:
    :show-inheritance:

spux.plot.mpl\_utils module
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.plot.mpl_utils
    :members:
    :undoc-members:
    :show-inheritance:

spux.plot.profile module
~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.plot.profile
    :members:
    :undoc-members:
    :show-inheritance:


