spux.processes package
======================

.. automodule:: spux.processes
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.processes.ornsteinuhlenbeck module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.processes.ornsteinuhlenbeck
    :members:
    :undoc-members:
    :show-inheritance:

spux.processes.precipitation module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.processes.precipitation
    :members:
    :undoc-members:
    :show-inheritance:

spux.processes.wastewater module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.processes.wastewater
    :members:
    :undoc-members:
    :show-inheritance:


