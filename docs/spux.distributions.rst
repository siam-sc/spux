spux.distributions package
==========================

.. automodule:: spux.distributions
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

spux.distributions.distribution module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.distributions.distribution
    :members:
    :undoc-members:
    :show-inheritance:

spux.distributions.multivariate module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.distributions.multivariate
    :members:
    :undoc-members:
    :show-inheritance:

spux.distributions.tensor module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: spux.distributions.tensor
    :members:
    :undoc-members:
    :show-inheritance:


