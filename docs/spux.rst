spux package
============

.. automodule:: spux
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    spux.distributions
    spux.drivers
    spux.executors
    spux.io
    spux.likelihoods
    spux.models
    spux.plot
    spux.processes
    spux.report
    spux.samplers
    spux.utils

