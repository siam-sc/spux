
.. _credits:

=======
Credits
=======

Development Lead
----------------

* Jonas Šukys <jonas.sukys@eawag.ch>
* Marco Bacci <marco.bacci@eawag.ch>


Contributors
------------

* Uwe Schmitt <uwe.schmitt@id.ethz.ch>
* Mikołaj Rybiński <mikolaj.rybinski@id.ethz.ch>
* Andreas Scheidegger <andreas.scheidegger@eawag.ch>
* Artur Safin <artur.safin@eawag.ch>
* Mira Kattwinkel <kattwinkel-mira@uni-landau.de>
* Marco Dal Molin <marco.dalmolin@eawag.ch>
* Simone Ulzega <ulzg@zhaw.ch>
* Peter Reichert <peter.reichert@eawag.ch>

Acknowledgments
---------------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage