# generate config
import script
del script

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (formats=['pdf'])

# plot datasets
plot.datasets ()

# plot marginal prior distributions
plot.priors (columns=6)

# plot error model distribution treating each dataset point as prediction
# and a random realization of parameters from prior distribution
plot.errors (labels = ['asellaqua', 'chironomi'])

# generate report
from spux.report import generate
authors = r'Marco Bacci'
generate.report (authors)
