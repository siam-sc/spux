
# specify prior distributions using stats.scipy for each parameter independently
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
from scipy import stats
from numpy import log, exp #, sqrt
from spux.io.loader import read_types_of_keys
#from spux.utils.transforms import rounding
from spux.distributions.tensor import Tensor

types_of_keys = {}
paramtypefl = "parameters.types"
types_of_keys = read_types_of_keys(infl=paramtypefl)

distributions = {}
pp = 0.5

# error
nn = 20   #the greater the wider the bell
ll = nn   #ll is mode
distributions ['k'] = stats.nbinom (n = nn, p = pp, loc=ll-nn)
#distributions ['k'] = stats.binom (n = nn, p = pp)

# global parameters
distributions ['invert_fresp']      = stats.lognorm (s=log(2),scale=exp(log(2.0)-0.5*log(2)**2))
distributions ['invert_fmort']      = stats.lognorm (s=log(2),scale=exp(log(0.8)-0.5*log(2)**2))
distributions ['invert_fmortEgg']   = stats.lognorm (s=log(2),scale=exp(log(0.9)-0.5*log(2)**2))
distributions ['invert_frespEgg']   = stats.lognorm (s=log(2),scale=exp(log(1.5)-0.5*log(2)**2))
distributions ['invert_minMass']    = stats.lognorm (s=log(2),scale=exp(log(1e-04)-0.5*log(2)**2))
distributions ['invert_fBasalSd']   = stats.lognorm (s=log(2),scale=exp(log(0.1)-0.5*log(2)**2))

# asellaqua
distributions ['asellaqua_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['asellaqua_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['asellaqua_kDens']           = stats.lognorm (s=log(2),scale=exp(log(6.0)-0.5*log(2)**2))
distributions ['asellaqua_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(1.75)-0.5*log(2)**2))
nn = 60
ll = nn
distributions ['asellaqua_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['asellaqua_eggM']            = stats.lognorm (s=log(2),scale=exp(log(7.41e-06)-0.5*log(2)**2))
distributions ['asellaqua_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.00103)-0.5*log(2)**2))

# chironomi
distributions ['chironomi_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['chironomi_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['chironomi_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['chironomi_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
nn = 750
ll = nn
distributions ['chironomi_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['chironomi_eggM']            = stats.lognorm (s=log(2),scale=exp(log(4.8e-07)-0.5*log(2)**2))
distributions ['chironomi_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.00038)-0.5*log(2)**2))

# dugesia
distributions ['dugesia_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['dugesia_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['dugesia_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['dugesia_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(4.5)-0.5*log(2)**2))
nn = 12
ll = nn
distributions ['dugesia_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['dugesia_eggM']            = stats.lognorm (s=log(2),scale=exp(log(1.9e-05)-0.5*log(2)**2))
distributions ['dugesia_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.00228)-0.5*log(2)**2))

# ephaem
distributions ['ephaem_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['ephaem_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['ephaem_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['ephaem_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(7.0)-0.5*log(2)**2))
nn = 38
ll = nn
distributions ['ephaem_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['ephaem_eggM']            = stats.lognorm (s=log(2),scale=exp(log(2.318e-05)-0.5*log(2)**2))
distributions ['ephaem_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.00169)-0.5*log(2)**2))

# gastro
distributions ['gastro_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['gastro_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['gastro_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['gastro_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(4.0)-0.5*log(2)**2))
nn = 315
ll = nn
distributions ['gastro_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['gastro_eggM']            = stats.lognorm (s=log(2),scale=exp(log(2.488e-05)-0.5*log(2)**2))
distributions ['gastro_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.005)-0.5*log(2)**2))

# hetero
distributions ['hetero_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['hetero_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['hetero_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['hetero_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(2.75)-0.5*log(2)**2))
nn = 509
ll = nn
distributions ['hetero_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['hetero_eggM']            = stats.lognorm (s=log(2),scale=exp(log(3.214e-05)-0.5*log(2)**2))
distributions ['hetero_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.01528)-0.5*log(2)**2))

# odonat
distributions ['odonat_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['odonat_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['odonat_kDens']           = stats.lognorm (s=log(2),scale=exp(log(4.0)-0.5*log(2)**2))
distributions ['odonat_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(2.0)-0.5*log(2)**2))
nn = 25
ll = nn
distributions ['odonat_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['odonat_eggM']            = stats.lognorm (s=log(2),scale=exp(log(1.9e-05)-0.5*log(2)**2))
distributions ['odonat_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.01207)-0.5*log(2)**2))

# oligochet
distributions ['oligochet_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['oligochet_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['oligochet_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.5)-0.5*log(2)**2))
distributions ['oligochet_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(1.5)-0.5*log(2)**2))
nn = 600
ll = nn
distributions ['oligochet_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['oligochet_eggM']            = stats.lognorm (s=log(2),scale=exp(log(1.35e-06)-0.5*log(2)**2))
distributions ['oligochet_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.001)-0.5*log(2)**2))

# simulati
distributions ['simulati_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['simulati_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['simulati_kDens']           = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['simulati_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(9.0)-0.5*log(2)**2))
nn = 63
ll = nn
distributions ['simulati_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['simulati_eggM']            = stats.lognorm (s=log(2),scale=exp(log(0.00020938)-0.5*log(2)**2))
distributions ['simulati_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.072273333)-0.5*log(2)**2))

# tricho
distributions ['tricho_fBasal']          = stats.lognorm (s=log(2),scale=exp(log(1.0)-0.5*log(2)**2))
distributions ['tricho_fIngest']         = stats.lognorm (s=log(2),scale=exp(log(15.0)-0.5*log(2)**2))
distributions ['tricho_kDens']           = stats.lognorm (s=log(2),scale=exp(log(6.0)-0.5*log(2)**2))
distributions ['tricho_fGrowthEgg']      = stats.lognorm (s=log(2),scale=exp(log(2.75)-0.5*log(2)**2))
nn = 500
ll = nn
distributions ['tricho_clutchSize_mean'] = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
distributions ['tricho_eggM']            = stats.lognorm (s=log(2),scale=exp(log(9.3e-07)-0.5*log(2)**2))
distributions ['tricho_adultM_mean']     = stats.lognorm (s=log(2),scale=exp(log(0.00029)-0.5*log(2)**2))

# construct a joint distribution for a vector of independent parameters by tensorization
prior = Tensor (distributions, types_of_keys)
