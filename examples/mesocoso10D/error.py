from scipy import stats
from spux.distributions.tensor import Tensor

# define an error model
class Error (object):

    # evaluate the (log-)likelihood of dataset given predictions and parameters
    def distribution (self, prediction, parameters):

        # specify error distributions using stats.scipy for each observed variable independently
        # available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
        warngiven = False
        kk = max(round(parameters['k']),1)
        distributions = {}

        for key in prediction.keys():
            if key.find('_scaled') != -1:
                continue
            str = key + '_scaled'
            if str in prediction.keys():
                distributions [key] = stats.nbinom( n = kk, p = kk / ( kk + max(0.1, prediction[str]) ) )
            else:
                if not warngiven:
                    print(":: WARNING: No scaled information for {}. Hope it's a plot.".format(str))
                    warngiven = True
                distributions [key] = stats.nbinom( n = kk, p = kk / ( kk + max(0.1, prediction[key]) ) )

        # overwrite methods if discrete distributions are in use
        for key in distributions:
            distributions [key].pdf = distributions [key].pmf
            distributions [key].logpdf = distributions [key].logpmf

        # construct a joint distribution for a vector of independent parameters by tensorization
        distribution = Tensor (distributions)

        return distribution

    def transform (self, observations, parameters):

        return observations.round ()

error = Error ()
