import os
import pandas
streams = ['1', '6', '7', '9', '13', '15', '16', '17', '18', '20']
columns = ['date',"Time",'stream','asellaqua','chironomi','dugesia','ephaem','gastro','hetero','odonat','oligochet','simulati','tricho']
datasets = {}
for stream in streams:
    datasets[stream] = pandas.read_csv (os.path.join ('datasets', 'obs_stream_%s_orig_area.dat' % stream), sep='\s+', index_col=1, skiprows=0, usecols=columns)
    del datasets [stream] ['date']
    del datasets [stream] ['stream']
