#!/bin/bash -l
#
#SBATCH --account=d97
#SBATCH --job-name="spx10D"
#SBATCH --time=00:30:00
#SBATCH --ntasks=126
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --distribution=block
#
module load cray-python/3.6.5.1
module load java/jdk1.8.0_51
export PYTHONPATH=/users/${USER}/sfw/spux_dev_marco:${PYTHONPATH}
TMPDIR=/scratch/snx3000/${USER}/spux_dev_marco/10D/a1
OUTDIR=/scratch/snx3000/${USER}/spux_dev_marco/10D/a1
#
if [ ! -d $TMPDIR ] ; then
  mkdir $TMPDIR
fi
if [ ! -d $OUTDIR ] ; then
  mkdir $OUTDIR
fi
#
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
#
srun --ntasks=${SLURM_NTASKS} --hint=nomultithread --cpu-bind=none python3 -m mpi4py script_parallel.py legacy >& ${OUTDIR}/tmplog
#
