# === CONNECTOR INITIALIZATION

from spux.executors.mpi4py.connectors import utils
connector = utils.select ('auto', verbosity = 1)

# === SAMPLER with attached executors

from script_executors_short import sampler

# === SAMPLING

# SANDBOX
# for compute clusters, set "target" path to scratch file system -
# then a symlink named "path" will be created instead
from spux.utils.sandbox import Sandbox
sandbox = Sandbox (path = "sandbox", target = None, template = 'input')

# SEED
from spux.utils.seed import Seed
seed = Seed (1)

# init executor
sampler.executor.init (connector)

# setup sampler
sampler.setup (sandbox=sandbox, verbosity=4, index=0, seed=seed, trace=0)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (12)

# exit executor
sampler.executor.exit ()
