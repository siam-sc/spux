
# === distribution for the initial model values

# specify distributions for each needed initial variable accordingly
from scipy import stats
from spux.distributions.tensor import Tensor

pp = 0.5
distributions = {}

nn = 338
ll = nn
distributions ['asellaqua_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 97
ll = nn
distributions ['chironomi_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 61
ll = nn
distributions ['dugesia_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 91
ll = nn
distributions ['ephaem_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 136
ll = nn
distributions ['gastro_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 40
ll = nn
distributions ['hetero_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 48
ll = nn
distributions ['odonat_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 141
ll = nn
distributions ['oligochet_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 47
ll = nn
distributions ['simulati_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)
nn = 14
ll = nn
distributions ['tricho_nInitm2']         = stats.nbinom (n = nn, p = pp, loc=ll-nn) #stats.binom (n = nn, p = pp)

# construct a tensor distribution for all variables
initial = Tensor (distributions)
