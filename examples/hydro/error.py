"""Define an error model for observations"""
from scipy import stats
from copy import deepcopy as copy

from spux.distributions.tensor import Tensor

from spux.processes.precipitation import Precipitation
from precipitation import precipitation_coefficients
h = Precipitation (*precipitation_coefficients)

# transformation function accounting for heteroscedasticity in output measurement errors
import numpy
numpy.seterr (over = 'ignore')
beta = 25 # [L/s]
alpha = 50 # [L/s]
g = lambda y : beta * numpy.log (numpy.sinh ((alpha + y) / beta))
g0 = g (0)
dg = lambda y : 1.0 / numpy.tanh ((alpha + y) / beta)
y = lambda g : beta * numpy.arcsinh (numpy.exp (g / beta)) - alpha

# define an error model
class Error (object):

    # return an error model (distribution) for the specified prediction and parameters
    def distribution (self, prediction, parameters):

        # specify error distributions using stats.scipy for each observed variable independently
        # available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
        distributions = {}
        distributions [r'$\xi$'] = stats.norm (loc = prediction [r'$\xi$'], scale = parameters [r'$\sigma_\xi$'])
        a = (g0 - prediction ['g']) / parameters [r'$\sigma_y$']
        distributions ['g'] = stats.truncnorm (a = a, b = float ('inf'), loc = prediction ['g'], scale = parameters [r'$\sigma_y$'])

        # construct a joint distribution for a vector of independent parameters by tensorization
        distribution = Tensor (distributions)

        return distribution

    # custom transformation function for the observation (prediction or dataset)
    def transform (self, observation, parameters):

        observation = copy (observation)
        observation [r'$\xi$'] = h.inverse (observation ['x'])
        del observation ['x']
        observation ['g'] = g (observation ['y'])
        del observation ['y']
        return observation

error = Error ()
