# generate config
import script
del script

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib ()

# plot datasets
plot.datasets ()

# plot marginal prior distributions
plot.priors ()

# plot error model distribution treating each dataset point as prediction
# and a random realization of parameters from prior distribution
plot.errors ()

# plot distributions for the initial model values
from initial import initial, initialize
from datasets import datasets
samples = { name : initialize (dataset.iloc [0]) for name, dataset in datasets.items () }
plot.distributions (initial, color='dimgray', samples=samples, suffix='-initial')

# generate report
from spux.report import generate
generate.report (authors = r'Jonas {\v S}ukys')