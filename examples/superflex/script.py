# === Superflex model

#set path to DLL
so_path = './lib/superflex_spux'

# construct model
from superflex import SuperSpuxFlex
from datasets import warmup_time
#tau = 10  #28*24*60*60 #28 days, 24 hours, 60 minutes, 60 seconds is a time step afaik
dt = 1    #no idea
model = SuperSpuxFlex(dt=dt,sandboxing=1,so_path=so_path,twarmup=warmup_time)

# === SPUX

# LIKELIHOOD
# construct likelihood - for marginalization in for stochastic models, use Particle Filter
from spux.likelihoods.pf import PF
likelihood = PF (particles=4)

# REPLICATES
# use Replicates likelihood to combine above likelihood with multiple independent data sets
from spux.likelihoods.replicates import Replicates
replicates = Replicates ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=4, attempts=10)

# ASSEMBLE ALL COMPONENTS
from error import error
from datasets import datasets
from prior import prior
likelihood.assign (model, error)
replicates.assign (likelihood, datasets)
sampler.assign (replicates, prior)