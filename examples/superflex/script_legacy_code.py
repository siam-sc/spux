
## Run the model all at once -> time dependent parameters (actually they are constant)
#dimensions = model_all.load_model()
#model_all.load_states(np.zeros(dimensions[2])) #this is a model set-up
#
#out = model_all.run_model(parameters = np.array([[1, 0.01]*dimensions[0]]).reshape(dimensions[:-1]),
#                          tIni = 1, tFin = dimensions[0])
##print("got out: ",out)
#
## Description of out:
## out[:, 0] = precipitation (input)
## out[:, 1] = potential evapotranspiration (input)
## out[:, 2] = observed streamflow (kind of an input)
## out[:, 3] = simulated streamflow (output, to be used for inference)
## out[:, 4] = reservoir state
#
#final_states_all = model_all.save_states()
#
## Description of the states
## final_states_all[0] = Simulated streamflow (all the model)
## final_states_all[1] = Simulated streamflow (HRU 1)
## final_states_all[2] = Corrected rainfall
## final_states_all[3] = Actual evapotranspiration
## final_states_all[4] = Reservoir state
#
#
## Run the model step_by_step
#
#model_steps = SuperSpuxFlex()
#dimensions = model_steps.load_model()
#model_steps.load_states(np.zeros(dimensions[2]))
#
## First half
#out1 = model_steps.run_model(parameters = np.array([[1, 0.01]*50]).reshape((50, 2)),
#                             tIni = 1, tFin = 50)
#
##print("got out1: ",out1)
#new_states = model_steps.save_states()
#
## Note that if we don't load the states the model continues with the states at
## the last timestep of the previous run. It is ok if we continue running the
## model. It is not right if the model re-runs.
#model_steps.load_states(new_states)
#
#out2 = model_steps.run_model(parameters = np.array([[1, 0.01]*50]).reshape((50, 2)),
#                             tIni = 51, tFin = 100)
##print("got out2: ",out2)
#final_states_steps = model_steps.save_states()
#
