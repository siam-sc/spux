BATEAU_ARG_FILE_V1.1
RunTag='Lac_S01_T28'; Inferenz='BASIC'[10]
[[date='20171123' and time='173123.696']]; infoStr='OptimisedParameters[mometh=2]'
! ===
Arguments [nArg=3]       ! [NB1: note extra 'SDEV' column]
'(2(1x,es22.14e3),2x,a,i4,a)' ! format statement
  7.16292659447252E-001 -9.99999999000000E+008  arg[   1] 'U1W%Cmlt_E, -'
 -7.54861976836751E+000 -9.99999999000000E+008  arg[   2] 'U1W%K_Qq_FR, mm^b/t'
  2.42830934350777E+000 -9.99999999000000E+008  arg[   3] 'sdevResid'
! ===
  -1.26097833797275E+03  ! value of function
                    168  ! function callz
! ---
! NB2: The information above describes optimization performance
! and does not conform to BATEAU ARG file formatting
! ===
! NB3: OTS information not available
! ===
! ISSUE REPORT: This MODAL file is generated using SUB 'writeXinfo'
! The file format allows loading it as an ARG file into BATEAU
! but it does not contain the 'outQ' and 'OTS' information
! If you need this missing info:
! 1. load this file as an ARG file into BATEAU
! 2. use the 'writeArgFile' functionality to generate a native ARG file
! ===
