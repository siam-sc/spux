# flake8: noqa
from numpy import log
# set initial parameters
initial = {}
initial ['log_Cmlt_E'] = log(1)
initial ['log_K_Qq_FR'] = log(0.01)
initial [r'$\sigma$'] = 0.1

