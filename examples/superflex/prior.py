
# specify prior distributions using stats.scipy for each parameter independently
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
import numpy # noqa: F401
from scipy import stats
from spux.distributions.tensor import Tensor

# limits are set in Case_Studies/Lacmalac/flexConfig/modLum/flexConfig_S01.dat
# values are to reproduce table 1 of https://doi.org/10.1002/2017WR021616

# with loc and scale in uniform distribution, we obtain the uniform dist. on [loc, loc + scale]
distributions = {}
#distributions ['C']        = stats.uniform (loc=0.1,scale=3.0-0.1)
distributions ['log_Cmlt_E']     = stats.uniform (loc=numpy.log(0.1), scale=numpy.log(3.0)-numpy.log(0.1))
#distributions ['K']        = stats.uniform (loc=10**-4, scale=0.1-10**-4)
distributions ['log_K_Qq_FR']     = stats.uniform (loc=numpy.log(10**-4), scale=numpy.log(0.1)-numpy.log(10**-4))
#distributions [r'$\sigma$'] = stats.lognorm (scale=numpy.exp(3), s=1)
distributions [r'$\sigma$'] = stats.uniform (loc=10**-6,scale=6-10**-6)
distributions [r'$\tau$'] = stats.lognorm ( s=numpy.log(2), scale=numpy.exp(numpy.log(10)-0.5*numpy.log(2)**2) )
distributions[r'$\chi_0$'] = stats.norm(loc=0, scale=1)
lambada = 0.1
distributions[r'$S_0$'] = stats.expon(scale=1/lambada)


### old marco_superflex_tdp ###
# limits are set in Case_Studies/Lacmalac/flexConfig/modLum/flexConfig_S01.dat
# with loc and scale, we obtain the uniform dist. on [loc, loc + scale]

#distr_support = {}
#distributions = {}

#distr_support ['C'] = (0.1,3.0)
#distributions ['C']     = stats.uniform (loc=distr_support['C'][0], scale=distr_support['C'][1]-distr_support['C'][0])

#distr_support ['K'] = (10**-6,10-10**-6)
#distributions ['K']     = stats.uniform (loc=distr_support['K'][0], scale=distr_support['K'][1]-distr_support['K'][0])

#distr_support [r'$\sigma$'] = (0.5,10)
#distributions [r'$\sigma$'] = stats.uniform (loc=distr_support[r'$\sigma$'][0], scale=distr_support[r'$\sigma$'][1] - distr_support[r'$\sigma$'][0])
# distributions [r'$\sigma$'] = stats.lognorm (scale=numpy.exp(3), s=1)
###############################

# construct a joint distribution for a vector of independent parameters by tensorization
prior = Tensor (distributions)
