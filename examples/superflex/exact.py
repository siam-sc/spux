# set exact parameters
from numpy import log
#
exact = {}
exact ['log_Cmlt_E'] = log(1)
exact ['log_K_Qq_FR'] = log(10**-3)
exact [r'$\sigma$'] = 2
