The streamflow data in the file Lacmalac_28.dat have been generated running the single reservoir model (Q=kS^2) with:

C = 1.0
k = 10e-3

The error model is BoxCox AR1 with "ll_BC_AR1(obs, mod, pars)":

sigma = 2.0
lambda = 0.5
phi = 0.5 (autocorrelation parameter)

The data used for inference goes from the timestep 1960-2-27 (27th) to 2010-2-20 (679th). Values until the 26th were used for warm-up.
All the input data are synthetic.

Data used in the paper https://doi.org/10.1002/2017WR021616, experiment 1.