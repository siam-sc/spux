# === CONNECTOR INITIALIZATION

from spux.executors.mpi4py.connectors import utils
connector = utils.select ('auto')

# === SAMPLER with attached executors

from script_executors import sampler

# === SAMPLING

# SANDBOX
# for compute clusters, set "target" path to scratch file system -
# then a symlink named "path" will be created instead
from spux.utils.sandbox import Sandbox
#sandbox = Sandbox (path = "simlink", target = '/home/mbacci/temp/spuxrun/superflex')
sandbox = Sandbox (path = 'sandbox', target = None)

# SEED
from spux.utils.seed import Seed
seed = Seed (2)

# init executor
sampler.executor.init (connector)

# setup sampler
sampler.setup (sandbox=sandbox, verbosity=1, index=0, seed=seed, trace=0, outputdir='output', informative=1)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (8)

# exit executor
sampler.executor.exit ()