import os
script = os.path.realpath (__file__)
directory, name = os.path.split (script)
inputdir = os.path.join (directory, 'datasets/Lacmalac/data')

import pandas
streams = ['28']
columns = ['Q']
datasets = {}

warmup_time = 0 #skip up to this line included

for stream in streams:

    if warmup_time == 0:
        datasets[stream] = pandas.read_csv (os.path.join (inputdir, 'Lacmalac_%s_synth.dat' % stream), sep="\s+", index_col=None, usecols=columns, skiprows=6) # noqa: W605
        Qlast = datasets [stream] ['Q'] .copy (deep=1)
        Qlast = Qlast.shift (1, fill_value=Qlast.iloc[0]) #duplicate or use [-1]? boh...
        datasets [stream] ['Qlast'] = Qlast
        datasets[stream].index += 1
    else:
        tmp = pandas.read_csv (os.path.join (inputdir, 'Lacmalac_%s_synth.dat' % stream), sep="\s+", index_col=None, usecols=columns, skiprows=6) # noqa: W605
        datasets[stream] = tmp.iloc[warmup_time:]
        Qlast = datasets [stream] ['Q'] .copy (deep=1)
        fillval = tmp['Q'].iloc[warmup_time-1]
        Qlast = Qlast.shift (1, fill_value=fillval)
        datasets [stream] ['Qlast'] = Qlast
        datasets[stream].index += 1

    datasets[stream].reindex(sorted(datasets[stream].columns), axis=1)
    datasets[stream].index.names = ["Time"]
    print(datasets[stream])
