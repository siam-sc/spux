# === SCRIPT

from script import sampler

# === SAMPLING

# SANDBOX
# for compute clusters, set "target" path to scratch file system -
# then a symlink named "path" will be created instead
from spux.utils.sandbox import Sandbox
sandbox = Sandbox (path = "simlink", target = '/home/mbacci/temp/spuxrun/superflex')

# SEED
from spux.utils.seed import Seed
seed = Seed (2)

# init executor
sampler.executor.init ()

# setup sampler
sampler.setup (verbosity=5, index=0, seed=seed)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (8)

# exit executor
sampler.executor.exit ()
