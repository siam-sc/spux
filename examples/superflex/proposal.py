# flake8: noqa

# specify proposal distributions using stats.scipy for each parameter independently
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
from scipy import stats
distributions = {}
distributions ['log_K_Qq_FR']     = stats.norm (loc=0, scale=0.01)
distributions ['log_Cmlt_E']     = stats.norm (loc=0, scale=0.01)
distributions [r'$\sigma$'] = stats.norm (loc=0, scale=0.01)

# construct a joint distribution for a vector of independent parameters by tensorization
from spux.distributions.tensor import Tensor
proposal = Tensor (distributions)
