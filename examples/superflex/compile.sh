#!/bin/bash
#
mkdir lib
cd lib
if [ $? == 0 ] ; then
  rm *
else
  echo "something is wrong with compile.sh"
  exit 1
fi
#
gfortran -O3 -fPIC -shared -c ../src/tools/kinds_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/bateauDK_types_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/minpack_ff.f90
gfortran -O3 -fPIC -shared -c ../src/tools/types_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/utilities_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/bateauDK_infile_mod.f90
gfortran -O3 -fPIC -shared -c ../src/tools/numerix_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/spam_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c ../src/tools/cpsm_dmsl_kit.f90
gfortran -O3 -fPIC -shared -c -ffree-line-length-0 ../src/superflex/FLEX_stdDmdl_dmsl_mod_corr_rev_distP.f90
gfortran -O3 -fPIC -shared -c ../src/tools/driver_FLEX_mod.f90
gfortran -O3 -fPIC -shared -c ../src/main/main_FLEX_dll_C.f90
gfortran -O3 -fPIC -shared *.o -o superflex_spux

