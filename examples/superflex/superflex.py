"""
This code wrap the SUPERFLEX modelling framework using the C dll version.
Use the compile.sh file to compile the code.

Methods:
    load_model : loads SUPERFLEX and returns useful dimensions
    run_model : runs SUPERFLEX and returns the output
"""
# flake8: noqa
import sys
import ctypes
import numpy
#import pandas
import time
import cloudpickle
#import gc
#from prior import distr_support

from spux.models.model import Model
from spux.utils.annotate import annotate

from scipy import stats # noqa: F401
from spux.processes.ornsteinuhlenbeck import OrnsteinUhlenbeck #, OrnsteinUhlenbeck_numba_spec

class SuperSpuxFlex(Model):

    states = None

    def __init__(self, dt = 1, sandboxing = 1, so_path = None, twarmup = 0):

        #print("within __init__ of superflex.py")
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        #sanity checks
        assert so_path is not None, ":: Fatal: please, provide a valid path to DLL (so_path). Got: {}".format(so_path)
        assert twarmup >= 0, ':: Fatal: warm-up time must be positive. Got: {}'.format(twarmup)

        #self.tau = numpy.float64(tau)
        self.dt = numpy.float64(dt)
        self.sandboxing = sandboxing
        self.so_path = so_path
        self.twarmup = twarmup
        #self.run_ready = 0

        #end = time.time()
        #print("__init__ of superflex took: ",end-start)
        #print("done with __init__ of superflex.py")
        #sys.stdout.flush()

    # Comment from Jonas: this method is no longer present for Model class
    # def setup(self, sandbox, verbosity, seed, informative, trace):

    #     #print("within setup of superflex.py")
    #     #sys.stdout.flush()
    #     #import pdb; pdb.set_trace()
    #     #start = time.time()

    #     Model.setup(self, sandbox, verbosity, seed, informative, trace)

    #     #if not self.run_ready and self.states is None: #we are here before init
    #     #    Model.setup(self, sandbox, verbosity, seed)
    #     #    return
    #     #elif self.run_ready and self.states is not None: #particle with everything
    #     #    Model.setup(self, sandbox, verbosity, seed)
    #     #    return
    #     #elif self.run_ready and self.states is None: #we are here before load
    #     #    Model.setup(self, sandbox, verbosity, seed)
    #     #    return
    #     #elif not self.run_ready and self.states is not None: #after load
    #     #    Model.setup(self, sandbox, verbosity, seed)
    #     #    tmpdim = self.dimensions
    #     #    self.dimensions = self.get_mdl_dims()
    #     #    assert tmpdim == self.dimensions, ": Fatal :: got inconsistent self.dimensions in setup of superflex. This is a bug"
    #     #else:
    #     #    raise ValueError("Wrong combo in setup (2)")

    #     #end = time.time()
    #     #print("setup of superflex took: ",end-start)
    #     #print("done with __init__ of superflex.py")
    #     #sys.stdout.flush()

    def free_mem(self):

        #print("within free_mem of superflex.py")
        #sys.stdout.flush()

        assert hasattr(self,'superflex'), "Fatal. In free_mem of superflex, got no superflex."

        self.superflex.clnsfmd.argtypes = None
        self.superflex.clnsfmd.restype = None
        self.superflex.clnsfmd()

        #print("done with free_mem of superflex.py")
        #sys.stdout.flush()

    def init(self, inputset, parameters):

        #print("within init of superflex.py",self.twarmup)
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        self.inputset = inputset
        #self.states = None #useless
        self.parameters = parameters

        #print("in init of superflex.py 0")
        #sys.stdout.flush()
        Model.init(self, inputset, parameters)

        # get initial time and make sure it is a float
        self.t = numpy.float64(0.0)

        #if self.run_ready:
        #    print("freeing superflex memory")
        #    self.free_mem()

        #self.run_ready = 1

        #print("in init of superflex.py 1")
        #sys.stdout.flush()
        self.dimensions = self.get_mdl_dims()
        #print("in init of superflex.py 2")
        #sys.stdout.flush()

        # initial values
        #xi0 = numpy.float(self.rng.normal(loc=0, scale=1)) #now in prior
        #S0 = self.rng.uniform(low=0, high=10)
        #print("****************************")
        #print("xi0: ",xi0)
        #print("S0: ",S0)
        #print("****************************")
        self.states = numpy.full(self.dimensions[2], self.parameters[r'$S_0$']) #numpy.zeros(self.dimensions[2])
        #print("in init of superflex.py 3")
        #sys.stdout.flush()

        # stochastic process
        self.OU = OrnsteinUhlenbeck (self.parameters[r'$\tau$']) #self.tau)
        self.OU.init (self.t, numpy.float64(self.parameters[r'$\chi_0$']))

        if self.twarmup == 0:
            return
        else:
           for t in range(1, self.twarmup + 1):
               #print('warming up:',t)
               self.run(t)

        #end = time.time()
        #print("init of superflex took: ",end-start)
        #print("done with init of superflex.py")
        #sys.stdout.flush()

    def get_mdl_dims(self): #so_path=/home/mbacci/sfw/spux/examples/superflex/lib/superflex_spux

        """
        This method initializes the model
        """

        #print("within get_mdl_dims of superflex.py")
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        # Load the dll
        #if not hasattr(self, 'superflex'):
        #sys.stdout.flush()
        if 1: #not hasattr(self, 'superflex'):
            self.superflex = ctypes.cdll.LoadLibrary(self.so_path)
            #print("SUPERFLEX INTERFACE WITH ROUTINES: ")
            #print(dir(self.superflex))

        #sys.stdout.flush()
        # Define the outputs of the load library
        nT_dum = ctypes.c_int()
        nPar_dum = ctypes.c_int()
        nParTime_dum = ctypes.c_int()
        nStates_dum = ctypes.c_int()
        nOut_dum = ctypes.c_int()
        err_dum = ctypes.c_int()

        # Define the interface to the library
        #if not hasattr(self.superflex, 'lsfmd'): #segfault if this switch is on
        #sys.stdout.flush()
        if 1:
            self.superflex.lsfmd.argtypes = [ctypes.POINTER(ctypes.c_int)]*6
            self.superflex.lsfmd.restype = None

        # Load the model
        #sys.stdout.flush()
        self.superflex.lsfmd(nT_dum, nPar_dum, nParTime_dum, nStates_dum, nOut_dum, err_dum)

        #sys.stdout.flush()
        if err_dum.value != 0:
            raise RuntimeError('Error in lsfmd: see SuperFlexErrorMessage.txt')

        # Extract the numeric value
        self.nT = nT_dum.value
        self.nPar = nPar_dum.value
        self.nParTime = nParTime_dum.value
        self.nStates = nStates_dum.value
        self.nOut = nOut_dum.value

        #end = time.time()
        #print("get_mdl_dims of superflex took: ",end-start)
        #print("done with get_mdl_dims of superflex.py")
        #sys.stdout.flush()

        # Return the dimensionality of the parameters
        return (self.nParTime, self.nPar, self.nStates)

    def run(self, tFin):

        """
        This method runs the model
        """
        #print("within run of superflex.py")
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        # Load the dll if required - should not be required anymore - setup does this
        #if not hasattr(self, 'superflex'):
        #    #print("within run of superflex.py, loading the ddl")
        #    startdll = time.time()
        #    self.superflex = ctypes.cdll.LoadLibrary(self.so_path)
        #    self.dimensions = self.get_mdl_dims()
        #    enddll = time.time()
        #    #print("setup of dll took: ",enddll-startdll)

        #if not hasattr(self, 'superflex'): #this is crucial if all particles are received
        #    self.superflex = ctypes.cdll.LoadLibrary(self.so_path)

        self.dimensions = self.get_mdl_dims()

        # sanity checks
        assert hasattr(self,'superflex'), "Fatal. In run of superflex, got no superflex."
        assert hasattr(self.superflex,'lsfmd'), "Fatal. In run of superflex, got superflex deprived of lsfmd routine."
        assert hasattr(self,"dimensions"), "Fatal, In run of superflex, got superflex deprived of dimensions."
        assert hasattr(self,"nT"), "Fatal, In run of superflex, got superflex deprived of nT."
        assert hasattr(self,"nPar"), "Fatal, In run of superflex, got superflex deprived of nPar."
        assert hasattr(self,"nParTime"), "Fatal, In run of superflex, got superflex deprived of nParTime."
        assert hasattr(self,"nStates"), "Fatal, In run of superflex, got superflex deprived of nStates."
        assert hasattr(self,"nOut"), "Fatal, In run of superflex, got superflex deprived of nOut."
        assert hasattr(self,"OU"), "Fatal, In run of superflex, got superflex deprived of OU."

        #if tIni >= tFin:
        #    raise ValueError('tIni must be smaller than tFin')

        if tFin < 1:
            raise ValueError('tFin must be positive')

        if tFin > self.nT:
            raise ValueError('tFin must be smaller than the total number of time steps ({} {})'.format(tFin, self.nT))

        # base class method does only printouts
        Model.run (self, tFin)

        # I want that python and superflex behave in the same way
        tIni_py = tFin - 1
        tFin_py = tFin
        ### old dev_marco_quiet ###
        #params = numpy.array( [self.parameters.values]*(tFin_py - tIni_py) )
        #params = numpy.atleast_2d( [ self.parameters['C'], self.parameters['K'] ]*(tFin_py - tIni_py) )
        #params = numpy.atleast_2d( [ numpy.exp(self.parameters['log_Cmlt_E']), numpy.exp(self.parameters['log_K_Qq_FR']) ]*(tFin_py - tIni_py) )
        ###########################
        xi = self.OU.evaluate (tFin_py, self.rng) #evaluate OU stochastic process

        ### old marco_superflex_tdp ###
        #update K parameter - how?
        #print("*****************")
        #print('zioone',tIni_py,tFin_py,xi,self.parameters['K'])
        #print("*****************")
        #sys.stdout.flush
        #self.parameters['K'] = self.parameters['K'] + numpy.exp(xi)
        #K = numpy.exp(xi)
        #if self.parameters['K'] < distr_support ['K'][0]:
        #    k = self.parameters['K']
        #    print("Warning. Brownian ratchet acting at the left hand side: ",xi,k)
        #    self.parameters['K'] = distr_support ['K'][0]
        #elif self.parameters['K'] > distr_support ['K'][1]:
        #    k = self.parameters['K']
        #    print("Warning. Brownian ratchet acting at the right hand side: ",xi,k)
        #    self.parameters['K'] = distr_support ['K'][1]
        #params = numpy.atleast_2d( [ self.parameters['C'], K ]*(tFin_py - tIni_py) )
        ###############################

        self.parameters['log_K_Qq_FR'] = xi
        params = numpy.atleast_2d( [ numpy.exp(self.parameters['log_Cmlt_E']), numpy.exp(self.parameters['log_K_Qq_FR']) ]*(tFin_py - tIni_py) )

        # Check if the parameter input has the right dimensionality
        if self.nParTime == 1: # time constant parameters
            if params.shape != (self.nParTime, self.nPar):
                raise ValueError('The parameters array has the wrong dimensionality. {} instead of {}'.format(
                                 params.shape, (self.nParTime, self.nPar)))
        else:
            if params.shape != (tFin_py - tIni_py, self.nPar):
                raise ValueError('The parameters array has the wrong dimensionality. {} instead of {}'.format(
                                 params.shape, (tFin_py - tIni_py, self.nPar)))

        if self.states is None:
            raise ValueError('No states available')

        # Prepare the parameters array
        # The Fortran code needs all the parameters (entire time series) and
        # they must have a valid value. So I set all at the first value of
        # my parameter set
        if self.nParTime == 1:
            par_all = params
        else:
            par_all = numpy.ones((self.nParTime, self.nPar)) * params[0, :]
            par_all[tIni_py : tFin_py, :] = params

        par_size = self.nPar * self.nParTime
        ParType = ctypes.c_double * par_size
        pars_list = par_all.reshape(par_size, order = 'F').tolist()
        pars_dum = ParType(*pars_list)       # to model run

        # Define the output array
        output_size = self.nT * self.nOut    # it's a flat array
        OutputType = ctypes.c_double * output_size
        output_dum = OutputType()            # to model run

        # Define sIni and sFin (1D arrays)
        sType = ctypes.c_double * self.nStates
        states_list = self.states.tolist()
        sIni_dum = sType(*states_list)       # to model run
        sFin_dum = sType()                   # to model run

        # Define the tIni, tFin, error
        tIni_dum = ctypes.c_int(tFin) #tIni) # to model run
        tFin_dum = ctypes.c_int(tFin)        # to model run
        err_dum = ctypes.c_int()             # to model run

        # Define the interface to the library
        self.superflex.rsfmd_pro.argtypes = [ctypes.POINTER(ctypes.c_double)] * 4 + [ctypes.POINTER(ctypes.c_int)] * 3
        self.superflex.rsfmd_pro.restype = None

        # Run the model
        #print("running the model from: ",tIni_dum," to: ",tFin_dum)
        #import pdb; pdb.set_trace()
        self.superflex.rsfmd_pro(output_dum, pars_dum, sIni_dum, sFin_dum, tIni_dum, tFin_dum, err_dum)

        if err_dum.value != 0:
            raise RuntimeError('Error in rsfmd: see SuperFlexErrorMessage.txt')

        # comply with error models that require knowledge of the past
        #if self.states is None: #useless - set to 0 above
        #    self.last = 0.0
        #else:
        if 1:
            self.last = self.states[0] #, 0 is Q of all HRUs of the model

        # Reshape the outputs
        output = numpy.array(output_dum[:]).reshape((self.nT, self.nOut), order = 'F')

        # Take care of the states
        self.states = numpy.array(sFin_dum[:])

        # order should not matter anymore thanks to alphabetic ordering in error
        prediction = annotate([output[tIni_py : tFin_py,3],[self.last]],['Q','Qlast'],tFin_py)

        #print("PREDICTION:: ",tFin_py,prediction)

        #end = time.time()
        #print("run of superflex took: ",end-start)
        #print("done with run of superflex.py")
        #sys.stdout.flush()

        return prediction

    def save(self):
        """
        This method returns the states of the model at the last timestep run
        """
        #print("within save of superflex.py")
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        if self.states is None:
            raise ValueError('No states available')

        #superflex is a dll, cannot be serialized by pickle: cherry-pick stuff
        stmp = {"states": self.states, "dimensions": self.dimensions, "nStates":self.nStates, "so_path": self.so_path, "parameters": self.parameters, "OU": self.OU}
        # all the rest useless as we re-init. dimensions and nStates only for check.
        #,"nT": self.nT,"nPar": self.nPar,"nParTime": self.nParTime, "nOut": self.nOut}

        sout = cloudpickle.dumps(stmp)

        #end = time.time()
        #print("save of superflex took: ",end-start)
        #print("done with save of superflex.py")
        #sys.stdout.flush()

        return sout

    def load(self, instates):
        """
        This method loads the states
        """
        #print("within load of superflex.py")
        #sys.stdout.flush()
        #import pdb; pdb.set_trace()
        #start = time.time()

        # set flag - we have lost initialization by copy(task) in ensemble_resample
        #self.run_ready = 0

        states = cloudpickle.loads (instates)

        self.nStates = states["nStates"]
        self.states = states["states"]

        if len(self.states) != self.nStates:
            raise ValueError('The states array has the wrong dimensionality. {} instead of {}'.format(
                             self.states.shape, self.nStates))

        self.parameters = states["parameters"]
        self.so_path = states["so_path"]
        self.dimensions = states["dimensions"]
        #self.nT = states["nT"]
        #self.nPar = states["nPar"]
        #self.nParTime = states["nParTime"]
        #self.nOut = states["nOut"]
        self.OU = states["OU"]

        if not hasattr(self,'superflex'): #this is crucial if all particles are received
            self.superflex = ctypes.cdll.LoadLibrary(self.so_path)
            tmpdim = self.dimensions
            self.dimensions = self.get_mdl_dims() #init library
            assert tmpdim == self.dimensions, ": Fatal :: got inconsistent self.dimensions in setup of superflex. This is a bug"

        #end = time.time()
        #print("load of superflex took: ",end-start)
        #print("done with load of superflex.py")
        #sys.stdout.flush()

        return self

    def exit(self):

        #print("within exit of superflex.py")
        #sys.stdout.flush()
        Model.exit(self)

        assert hasattr(self,'superflex'), "Fatal. In exit of superflex, got no superflex."
        #import pdb; pdb.set_trace()
        # Define the interface to the library
        #if hasattr(self,'superflex'):
        self.superflex.clnsfmd.argtypes = None
        self.superflex.clnsfmd.restype = None
        #print("was great meeting you")
        # Just, implode
        self.superflex.clnsfmd()
        #del self.superflex #makes superflex unavailable to other particles  #slow, useless

        #gc.collect() #slow, useless
        #print("but now I am long gone")
        #print("done with exit of superflex.py")
        #sys.stdout.flush()

        return None
