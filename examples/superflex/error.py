from scipy import stats
from spux.distributions.tensor import Tensor
from spux.io.loader import read_types_of_keys
import obserrs
import numpy

errmdlstr = "ll_BC_AR1" # "ll_SLS"

# define custom distribution object
class Distribution (object):

    def __init__(self, errmdl, types_of_keys, prediction, parameters):

        self.errmdl = errmdl
        self.prediction = prediction.reindex(sorted(prediction.keys()),axis=1)
        self.parameters = parameters

        if types_of_keys is None:
            raise ValueError(":: Fatal : Current status requires assigning labels. Please, specify and use a predictstypefl.")
        else:
            self.labels = list(types_of_keys.keys ())
            self.types = {}
            for key in types_of_keys:
                self.types [key] = types_of_keys [key]

    def pdf(self, dataset):
        dataset = dataset.reindex(sorted(dataset.keys()), axis=1)
        try:
            result = self.errmdl(dataset, self.prediction, self.parameters)
        except:
            raise ValueError(":: Fatal : Error model and parameters do seem not compatible. Some parameters missing? Or simply a bug.",dataset,self.prediction,self.parameters)
        return numpy.exp(result) #obserr always returns log

    def logpdf(self,dataset):
        dataset = dataset.reindex(sorted(dataset.keys()), axis=1)
        try:
            result = self.errmdl(dataset, self.prediction, self.parameters)
        except:
            raise ValueError(":: Fatal : Error model and parameters do seem not compatible. Some parameters missing? Or simply a bug.",dataset,self.prediction,self.parameters)
        return result


# define an error model
class Error (Distribution):

    def __init__ (self, errmdlname=None, predictstypefl=None):

        self.errmdlname = errmdlname
        self.predictstypefl = predictstypefl

        if self.errmdlname is not None:
            try:
                isinstance(self.errmdlname, str)
            except:
                raise ValueError(":: Fatal : Error model is not a string. This may be a bug.")
            try:
                self.errmdl = getattr(obserrs, self.errmdlname)
            except AttributeError:
                raise AttributeError(":: Fatal : Error model is not available. This may be a bug.")
        else:
            print(":: Warning: No error model given. A default one will be used.")
            self.errmdl = None

        if self.predictstypefl is not None:
            try:
                isinstance(self.predictstypefl, str)
            except AttributeError:
                raise ValueError(":: Fatal : predictstypefl is not a string. This may be a bug.")
            self.types_of_keys = read_types_of_keys(infl=predictstypefl)
        else:
            self.types_of_keys = None

    # return an error model (distribution) for the specified prediction and parameters
    def distribution (self, prediction, parameters):

        if self.errmdl is None:
            if self.types_of_keys is not None:
                raise ValueError(":: Fatal : errmdl is None but types_of_keys is not None. Chances are you don't know what you are doing.")
            self.types_of_keys = {'Q':'float'}
            distributions = {}
            distributions ['Q'] = stats.norm (loc=prediction['Q'], scale=parameters[r'$\sigma$'])
            # construct a joint distribution for a vector of independent parameters by tensorization
            distribution = Tensor (distributions, self.types_of_keys)
            return distribution
        else:
            return Distribution(self.errmdl, self.types_of_keys, prediction, parameters)

error = Error (errmdlname=errmdlstr, predictstypefl="predictions.types")
