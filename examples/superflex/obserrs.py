"""
This module contains the functions to calculate the log-likelihood.

All these functions have the same interface. For optimization use a lambda
function that wrap the negative likelihood (we have a minimizer but we want to
find the maximum):

    from scipy.optimize import minimize
    minimize(lambda x : -ll_SLS(obs, mod, x), x0 = ....)
"""

import numpy as np
np.seterr(all='raise')
from scipy.stats import norm

def ll_SLS(obs, mod, pars):
    """
    This function calculates the log-likelihood using the SLS error model.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : sigma

    Returns
    -------
    float
        Log-likelihood value
    """

    #import pdb; pdb.set_trace()

    sigma = pars[r'$\sigma$']

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    res = obs - mod

    pdf = norm.pdf(res, loc = 0, scale = sigma)

    return np.sum(np.log(pdf))

def ll_WLS(obs, mod, pars):
    """
    This function calculates the log-likelihood using the WLS error model.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : a
        pars[1] : b

    Returns
    -------
    float
        Log-likelihood value
    """
    a = pars['a']
    b = pars['b']

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    res = obs - mod

    pdf = norm.pdf(res/(a + b*mod), loc = 0, scale = 1)

    return np.sum(np.log(pdf) - np.log(a + b*mod))

def BC(data, lam, offset = 0):
    """
    Box-Cox transformation
    """
    return (np.power(data + offset, lam) - 1)/lam

def ll_BC(obs, mod, pars):
    """
    This function calculates the log-likelihood using SLS error model on top of
    Box-Cox transformed data.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : lambda
        pars[1] : sigma
        pars[2] : offset (optional)

    Returns
    -------
    float
        Log-likelihood value
    """
    lam = pars['lam']
    sigma = pars[r'$\sigma$']
    if len(pars) == 3:
        offset = pars['offset']
    else:
        offset = 0

    if lam <= 0:
        raise ValueError('lambda must be > 0')

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    obs_BC = BC(obs, lam, offset)
    mod_BC = BC(mod, lam, offset)

    res = obs_BC - mod_BC

    pdf = norm.pdf(res, loc = 0, scale = sigma)

    return np.sum(np.log(pdf) + np.log(np.power((obs + offset), lam - 1)))

def ll_BC_bias(obs, mod, pars):
    """
    This function calculates the log-likelihood using SLS error model on top of
    Box-Cox transformed data.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : lambda
        pars[1] : sigma
        pars[2] : offset
        pars[3] : a

    Returns
    -------
    float
        Log-likelihood value
    """
    lam = pars['lam']
    sigma = pars[r'$\sigma$']
    offset = pars['offset']
    a = pars['a']

    if lam <= 0:
        raise ValueError('lambda must be > 0')

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    obs_BC = BC(obs, lam, offset)
    mod_BC = BC(mod, lam, offset)

    res = obs_BC - a*mod_BC

    pdf = norm.pdf(res, loc = 0, scale = sigma)

    return np.sum(np.log(pdf) + np.log(np.power((obs + offset), lam - 1)))

def ll_BC_AR1(obs, mod, pars):
    """
    This function calculates the log-likelihood using SLS + AR1 error model on
    top of Box-Cox transformed data.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : lambda
        pars[1] : phi (autocorrelation)
        pars[1] : sigma
        pars[2] : offset (optional)

    Returns
    -------
    float
        Log-likelihood value
    """

    #import pdb; pdb.set_trace()

    #if "lam" in pars:
    #    lam = pars['lam']
    #else:
    lam = 0.5
    #if "phi" in pars:
    #    phi = pars['phi']
    #else:
    phi = 0.5
    #if r'$\sigma$' in pars:
    sigma = pars[r'$\sigma$']
    #else:
    #    raise ValueError(":: Fatal : Infer sigma or turn me off, come on!")

    #if "offset" in pars:
    #    offset = pars['offset']
    #else:
    offset = 0

    #if lam <= 0:
    #    raise ValueError('lambda must be > 0')

    #oindxQ = obs.index.get_loc ("Q")
    #oindxQlast = obs.index.get_loc ("Qlast")
    obs = np.array(obs)

    #mindxQ = mod.index.get_loc ("Q")
    #mindxQlast = mod.index.get_loc ("Qlast")
    mod = np.array(mod)

    #assert oindxQ == 1, "Fatal. oindxQ != 1"
    #assert mindxQ == 1, "Fatal. oindxQ != 1"
    #assert oindxQlast == 0, "Fatal. oindxQlast != 0"
    #assert mindxQlast == 0, "Fatal. oindxQlast != 0"

    obs_BC = BC(obs, lam, offset)
    mod_BC = BC(mod, lam, offset)

    res = obs_BC - mod_BC
    #if len(res) <= 1:
    #  raise ValueError('Error model requires knowledge of the past.')

    res_new = res[1:]
    res_old = res[:-1]
    obs_new = obs[1:]

    #print("res_new: ", res_new)
    try:
        pdf = norm.pdf(res_new - phi*res_old, loc = 0, scale = sigma)
        result = np.sum(np.log(pdf) + np.log(np.power((obs_new + offset), lam - 1)))
    except FloatingPointError as e:
        print(":: WARNING: log prob set to -inf due to underflow, ",e)
        result = -float('Inf')

    return result

def ll_log(obs, mod, pars):
    """
    This function calculates the log-likelihood using SLS error model on top of
    log transformed data.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : sigma

    Returns
    -------
    float
        Log-likelihood value
    """
    sigma = pars[r'$\sigma$']

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    res = np.log(obs) - np.log(mod)

    pdf = norm.pdf(res, loc = 0, scale = sigma)

    return np.sum(np.log(pdf) + np.log(np.power(obs, -1)))

def ll_SLS_AR1(obs, mod, pars):
    """
    This function calculates the log-likelihood using the SLS + AR1 error
    model.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : sigma
        pars[1] : phi (autocorrelation)

    Returns
    -------
    float
        Log-likelihood value
    """
    sigma = pars[r'$\sigma$']
    phi = pars['phi']

    oindxQ = obs.index.get_loc ("Q")
    oindxQlast = obs.index.get_loc ("Qlast")
    obs = np.array(obs)

    mindxQ = mod.index.get_loc ("Q")
    mindxQlast = mod.index.get_loc ("Qlast")
    mod = np.array(mod)

    assert oindxQ == 1, "Fatal. oindxQ != 1"
    assert mindxQ == 1, "Fatal. oindxQ != 1"
    assert oindxQlast == 0, "Fatal. oindxQlast != 0"
    assert mindxQlast == 0, "Fatal. oindxQlast != 0"

    res = obs - mod
    if len(res) <= 1:
      raise ValueError('Error model requires knowledge of the past.')

    res_new = res[1:]
    res_old = res[:-1]

    pdf = norm.pdf(res_new - phi*res_old, loc = 0, scale = sigma)

    return np.sum(np.log(pdf))

def ll_WLS_AR1_app2(obs, mod, pars):
    """
    This function calculates the log-likelihood using the WLS + AR1 error
    model. It uses the approach 2 in Evin 2013.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : a
        pars[1] : b
        pars[2] : phi (autocorrelation)

    Returns
    -------
    float
        Log-likelihood value
    """
    a = pars['a']
    b = pars['b']
    phi = pars['phi']
    sigma = np.sqrt(1 - np.square(phi))

    oindxQ = obs.index.get_loc ("Q")
    oindxQlast = obs.index.get_loc ("Qlast")
    obs = np.array(obs)

    mindxQ = mod.index.get_loc ("Q")
    mindxQlast = mod.index.get_loc ("Qlast")
    mod = np.array(mod)

    assert oindxQ == 1, "Fatal. oindxQ != 1"
    assert mindxQ == 1, "Fatal. oindxQ != 1"
    assert oindxQlast == 0, "Fatal. oindxQlast != 0"
    assert mindxQlast == 0, "Fatal. oindxQlast != 0"

    res = obs - mod
    if len(res) <= 1:
      raise ValueError('Error model requires knowledge of the past.')
    res_norm = res/(a + b * mod)

    res_norm_new = res_norm[1:]
    res_norm_old = res_norm[:-1]
    mod_new = mod[1:]

    pdf = norm.pdf(res_norm_new - phi*res_norm_old, loc = 0, scale = sigma)

    return np.sum(np.log(pdf) - np.log(a + b*mod_new))

def ll_WLS_AR1_app1(obs, mod, pars):
    """
    This function calculates the log-likelihood using the WLS + AR1 error
    model. It uses the approach 1 in Evin 2013.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : a
        pars[1] : b
        pars[2] : phi (autocorrelation)

    Returns
    -------
    float
        Log-likelihood value
    """
    a = pars['a']
    b = pars['b']
    phi = pars['phi']

    oindxQ = obs.index.get_loc ("Q")
    oindxQlast = obs.index.get_loc ("Qlast")
    obs = np.array(obs)

    mindxQ = mod.index.get_loc ("Q")
    mindxQlast = mod.index.get_loc ("Qlast")
    mod = np.array(mod)

    assert oindxQ == 1, "Fatal. oindxQ != 1"
    assert mindxQ == 1, "Fatal. oindxQ != 1"
    assert oindxQlast == 0, "Fatal. oindxQlast != 0"
    assert mindxQlast == 0, "Fatal. oindxQlast != 0"

    res = obs - mod
    if len(res) <= 1:
      raise ValueError('Error model requires knowledge of the past.')

    res_new = res[:1]
    res_old = res[:-1]
    mod_new = mod[:1]

    pdf = norm.pdf(res_new - phi*res_old, loc = 0, scale = a + b*mod_new)

    return np.sum(np.log(pdf))

def combine_ll(obs, mod, catchments, seasons, fun, pars):
    """
    This function wraps a log-likelihood function and make it possible to
    apply it to more catchments together.

    Parameters
    ----------
    obs : pd.DataFrame
        Observed data. First level of column names is the catchment, second
        level is the season.
    mod : pd.DataFrame
        Modelled data. First level of column names is the catchment, second
        level is the season.
    catchments : list
        List of catchments
    seasons : list
        List of seasons
    fun : function
        Wrapped likelihood function
    pars : list
        Parameters of the likelihood function

    Returns
    -------
    float
        Log-likelihood value
    """

    # Only important checks
    if not isinstance(catchments, list):
        raise TypeError('catchments must be of type list even if it is just one value')
    if not isinstance(seasons, list):
        raise TypeError('seasons must be of type list even if it is just one value')

    combinations = [(x, y) for x in catchments for y in seasons]

    ll = 0

    for comb in combinations:
        ll += fun(obs = np.array(obs[comb].values, dtype = 'float64'),
                  mod = np.array(mod[comb].values, dtype = 'float64'),
                  pars = pars)

    return ll

def ll_WLS_bias(obs, mod, pars):
    """
    This function calculates the log-likelihood using the WLS error model.

    Parameters
    ----------
    obs : np.array
        1D Array of observed data
    mod : np.array
        1D Array of modeled data
    pars : list
        List with the parameters.
        pars[0] : a
        pars[1] : b
        pars[2] : c
        pars[3] : d

    Returns
    -------
    float
        Log-likelihood value
    """
    a = pars['a']
    b = pars['b']
    c = pars['c']
    d = pars['d']

    obs = np.array(obs['Q'])
    mod = np.array(mod['Q'])

    res = (obs - c - d*mod)/(a + b*mod)

    pdf = norm.pdf(res, loc = 0, scale = 1)

    return np.sum(np.log(pdf) - np.log(a + b*mod))
