# === Superflex model

# set input directory path (contents will be copied to each sandbox)
inputpath = 'input'
#set path to DLL
so_path = './lib/superflex_spux'

# construct model
from superflex import SuperSpuxFlex
from datasets import warmup_time
model = SuperSpuxFlex(sandboxing=0,inputpath=inputpath,so_path=so_path,twarmup=warmup_time)

# === SPUX

# LIKELIHOOD
# construct likelihood - for marginalization in for stochastic models, use Particle Filter
from spux.likelihoods.standard import Standard
likelihood = Standard ()

# REPLICATES
# use Replicates likelihood to combine above likelihood with multiple independent data sets
from spux.likelihoods.replicates import Replicates
replicates = Replicates ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=4, attempts=10)

# ASSEMBLE ALL COMPONENTS
from error import error
from datasets import datasets
from prior import prior
likelihood.assign (model, error)
replicates.assign (likelihood, datasets)
sampler.assign (replicates, prior)
