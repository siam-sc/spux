# === IBM model

# path to model Java class
classpath = 'IBM-Bugs_OpenJDK.jar'

# set configuration
config = 'generalSettings_extFiltering.txt'

# construct model
from spux.models.ibm import IBM
model = IBM (config, classpath=classpath, paramtypefl="parameters.types", jvmpath=None) #jvmpath="/storage006/mbacci/jvm/jdk-11.0.1/lib/server/libjvm.so")

# === SPUX

# LIKELIHOOD
# construct likelihood - for marginalization in for stochastic models, use Particle Filter
from spux.likelihoods.pf import PF
likelihood = PF (particles=2)

# REPLICATES
# use Replicates likelihood to combine above likelihood with multiple independent data sets
from spux.likelihoods.replicates import Replicates
replicates = Replicates ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=6)

# ASSEMBLE ALL COMPONENTS
from error import error
from datasets import datasets
from prior import prior
likelihood.assign (model, error)
replicates.assign (likelihood, datasets)
sampler.assign (replicates, prior)
