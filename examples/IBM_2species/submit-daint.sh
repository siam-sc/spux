#!/bin/bash -l
#
#SBATCH --account=d97
#SBATCH --job-name="spxibm2"
#SBATCH --time=00:30:00
#SBATCH --ntasks=126
#SBATCH --ntasks-per-core=1
#SBATCH --cpus-per-task=1
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --distribution=block
#
module load cray-python/3.6.5.1
module load java/jdk1.8.0_51
export PYTHONPATH=/users/${USER}/sfw/spux_dev_marco:${PYTHONPATH}
export PMI_MMAP_SYNC_WAIT_TIME=7200
TMPDIR=/scratch/snx3000/${USER}/spux_dev_marco/IBM2/a1
OUTDIR=/scratch/snx3000/${USER}/spux_dev_marco/IBM2/a1
#
if [ ! -d $TMPDIR ] ; then
  mkdir $TMPDIR
fi
if [ ! -d $OUTDIR ] ; then
  mkdir $OUTDIR
fi
#
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
#
srun --ntasks=${SLURM_NTASKS} --hint=nomultithread python3 -m mpi4py script_parallel_emcee.py legacy >& ${OUTDIR}/tmplog
#srun --ntasks=${SLURM_NTASKS} --hint=nomultithread --cpu-bind=rank python3 -m mpi4py script_parallel_emcee.py legacy >& ${OUTDIR}/tmplog
#srun --ntasks=${SLURM_NTASKS} --hint=nomultithread --cpu-bind=none python3 -m mpi4py script_parallel_emcee.py legacy >& ${OUTDIR}/tmplog
#
