import numpy
#import sys

# === IBM BUGS model
# path to model Java class
# import pdb; pdb.set_trace()
classpath = 'IBM-Bugs_OpenJDK.jar'

# specify name of input file for model
config = 'generalSettings_extFiltering.txt'

# set input directory path (contents of which will be copied to each sandbox)
inputpath = 'input'

# construct IBM model (see 'ibm.py' in the current directory)
from spux.models.ibm import IBM

# prepare running dir (aka sandbox) for ibm1
from spux.utils import sandbox
sandbox1 = sandbox.Sandbox (path = "test/m1", target = None)

# prepare seed
from spux.utils import seed
seed1 = seed.Seed (4, name='sampler')

# construct IBM model object
ibm1 = IBM(config, classpath, inputpath, serialization="binary")

# setup IBM model
ibm1.setup(sandbox=sandbox1,verbosity=1,seed=seed1)

# set parameters and init
parameters = {}
parameters["prey_kDens"] = 26
parameters["predator_kDens"] = 16
inputset = None
ibm1.init(inputset, parameters)

# report to stdout
print("\n--- obs0 ---")
print("ibm1 t = 0", numpy.array(ibm1.model.observe()) )
print("--- done ---\n")

# run IBM model up to time = 1000
ibm1.run(time=1000)

# report observations
obs1000 = numpy.array(ibm1.model.observe())
print("\n--- obs1000 ---")
print("ibm1 t = 1000", obs1000)
print("--- done ---\n")

# === test for saving and loading model state
# save state
state = ibm1.save()

# run IBM model up to time = 2500
seed2 = seed.Seed (8, name='sampler')
ibm1.setup(sandbox=sandbox1,verbosity=1,seed=seed2)
ibm1.run(time=2500)

# report observations
obs2500 = numpy.array(ibm1.model.observe())
print("\n--- obs2500 ---")
print("ibm1 t = 2500", obs2500)
print("--- done ---\n")

# create new IBM model object
print("--- constructing ib2 ---")
ibm2 = IBM(config, classpath, inputpath, serialization="binary")
print("--- done ---\n")

#print("--- load state ---")
#ibm2.load(state) # state is test/m1/state.dat
#print("--- done ---\n")

# create new run folder and do setup
sandbox2 = sandbox.Sandbox (path = "test/m2", target = None)
# java model does not exist here yet
ibm2.setup(sandbox=sandbox2,verbosity=1,seed=seed2)

#print("--- load state ---")
#ibm2.load(state) # state is test/m1/state.dat
#print("--- done ---\n")

##############################
#import os
#from spux.drivers import java
#from spux.io import parameters as txt
ibm2.time = 1000
ibm2.sandbox.copyin (ibm2.inputpath)
#filenames = ["2spec_taxa_param.dat", "2spec_uni_param.dat"]
#path = os.path.join(ibm2.sandbox(), "input")
#for filename in filenames:
#    inputfile = os.path.join(path, filename)
#    available = txt.load(inputfile)
#    for label, value in parameters.items():
#        if label in available:
#            available [label] = [value] [0] #value [0] gives error in test noframework
#            txt.save(available, inputfile, delimiter="\t")
#ibm2.model = ibm2.Model (ibm2.seed.cumulative())
#ibm2.model.setPaths (ibm2.sandbox())
#ibm2.model.initModel([ibm2.config])
#ibm2.model.initSimulation() #open test/m2/output/out_2spec.csv
#ibm2.model.runSimulationInitExtPartFiltering() #writes first line of test/m2/output/out_2spec.csv
#####################################################################
##ibm2.init(parameters)
#
print("--- load state ---")
ibm2.load(state) # state is test/m1/state.dat
ibm2.setup(sandbox=sandbox2,verbosity=1,seed=seed2)

#ibm2.model.setPaths (ibm2.sandbox()) #added in load of ibm.py
print("--- done ---\n")

# report loaded observations
print("\n--- obs1000 ---")
print("ibm1 t = 1000", obs1000)
print("ibm2 t = 1000", numpy.array(ibm2.model.observe()) )
print( "ibm2 t = 1000", numpy.array(ibm2.model.observe()), numpy.array(ibm2.model.observe()) == obs1000)
test = (numpy.array(ibm2.model.observe()) == obs1000)
assert( test.all() )
print("--- done ---\n")

# run IBM model up to time = 2500
ibm2.run(time=2500)

# report observations - do they match?
print("\n--- obs2500 ---")
print("ibm1 t = 2500", obs2500)
print("ibm2 t = 2500", numpy.array(ibm2.model.observe()) )
print("ibm2 t = 2500", numpy.array(ibm2.model.observe()) == obs2500 )
test = (numpy.array(ibm2.model.observe()) == obs2500)
assert( test.all() )
print("--- done ---\n")
#
