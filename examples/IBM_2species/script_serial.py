# === SCRIPT

from script_emcee import sampler

# === SAMPLING

# SEED
from spux.utils.seed import Seed
seed = Seed (1)

# init executor
sampler.executor.init ()

# setup sampler
sampler.setup (verbosity=5, index=0, seed=seed)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (6)

# exit executor
sampler.executor.exit ()
