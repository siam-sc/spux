from scipy import stats
from spux.distributions.tensor import Tensor

# define an error model
class Error (object):

    # evaluate the (log-)likelihood of dataset given predictions and parameters
    def distribution (self, prediction, parameters):

        # specify error distributions using stats.scipy for each observed variable independently
        # available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
        k = 20
        distributions = {}
        distributions ['prey'] = stats.nbinom( n = k, p = k / ( k + round(prediction['prey']) ) )
        distributions ['predator'] = stats.nbinom( n = k, p = k / ( k + round(prediction['predator']) ) )

        # overwrite methods if discrete distributions are in use
        for key in distributions:
            distributions [key].pdf = distributions [key].pmf
            distributions [key].logpdf = distributions [key].logpmf

        # construct a joint distribution for a vector of independent parameters by tensorization
        distribution = Tensor (distributions)

        return distribution

    def transform (self, observations, parameters):

        return observations.round ()

error = Error ()
