import os
import pandas
streams = ['1', '2', '3']
columns = ['stream', 'run', 'timestep', 'prey', 'predator']
datasets = {}
for stream in streams:
    datasets[stream] = pandas.read_csv (os.path.join ('datasets', 'out_2Dsynth_%s.csv.new.err' % stream), sep=',', usecols=columns, index_col=2)
    del datasets [stream] ['run']
    del datasets [stream] ['stream']
