ROOT_PATH=$(cd "$(dirname $0)/../.."; pwd) # OS X and BSD compatible abs. path
export PYTHONPATH=$PYTHONPATH:${ROOT_PATH}
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LD_RUN_PATH

unset _JAVA_OPTIONS

if [ -z "$1" ]; then
    n=8
    script_ver=""
else
    n=$1
    script_ver=$1
fi

{ time -p mpiexec --oversubscribe -n ${n} python3 -m mpi4py script${script_ver}.py ${@:1} 2> mpiexec${script_ver}.err ; }  2> time.out
