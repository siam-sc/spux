# generate config
import script_emcee
del script_emcee

# import exact parameters
from exact import exact

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (exact = exact, formats = ['pdf'])

# plot datasets
plot.datasets ()

# plot marginal prior distributions
plot.priors ()

# plot error model distribution treating each dataset point as prediction
# and a random realization of parameters from prior distribution
plot.errors ()

# generate report
from spux.report import generate
authors = r'Marco Bacci'
generate.report (authors)

# # THIS IS NOT VERY USEFUL - DUPLICATES THE ABOVE - TO BE REMOVED
# # plot error model distribution for a manually chosen prediction
# # and a random realization of parameters from prior distribution
# # include several available observations from datasets
# from spux.utils.annotate import annotate
# prediction = annotate (data=[3000, 50], labels=['prey', 'predator'], time=0)
# from numpy.random import RandomState
# rng = RandomState (seed=1)
# parameters = prior.draw (rng)
# print ('Generated parameters:')
# print (parameters)
# from error import error
# distribution = error.distribution (prediction, parameters)
# samples = { name : error.transform (dataset.iloc [0], parameters) for name, dataset in datasets.items () }
# plot.distributions (distribution, color='spux_green', samples=samples, suffix='-error')

