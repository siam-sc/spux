"""Specify prior distributions using stats.scipy and optional parameters types file"""
# specify prior distributions using stats.scipy for each parameter independently
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html
from scipy import stats
from numpy import log, exp #, sqrt
from spux.io.loader import read_types_of_keys
#from spux.utils.transforms import rounding
from spux.distributions.tensor import Tensor

types_of_keys = {}
paramtypefl = "parameters.types"
types_of_keys = read_types_of_keys(infl=paramtypefl)

distributions = {}
distributions ['prey_kDens']     = stats.lognorm (s=log(2),scale=exp(log(16)-0.5*log(2)**2))
distributions ['predator_kDens'] = stats.lognorm (s=log(2),scale=exp(log(14)-0.5*log(2)**2))
n = 200
p = 0.5
distributions ['prey_nInitm2'] = stats.nbinom( n = n, p = p, loc=100-n)

prior = Tensor (distributions, types_of_keys)
