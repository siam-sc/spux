# flake8: noqa
from numpy import log
# set initial parameters
initial = {}
initial ['prey_kDens'] = 18
initial ['predator_kDens'] = 12
initial ['prey_nInitm2'] = 100
