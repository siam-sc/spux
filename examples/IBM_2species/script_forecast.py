# === IBM model

# path to model Java class
classpath = 'IBM-Bugs_OpenJDK.jar'

# set configuration
config = 'generalSettings_extFiltering.txt'

# construct model
from spux.models.ibm import IBM
model = IBM (config, classpath=classpath, paramtypefl="parameters.types", jvmpath=None) #jvmpath="/storage006/mbacci/jvm/jdk-11.0.1/lib/server/libjvm.so"

# === SPUX

# LIKELIHOOD
# construct likelihood - for marginalization in for stochastic models, use Particle Filter
from spux.likelihoods.pf import PF
likelihood = PF (particles=2, noresample=1)

# REPLICATES
# use Replicates likelihood to combine above likelihood with multiple independent data sets
from spux.likelihoods.replicates import Replicates
replicates = Replicates ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.forecast import FORECAST
sampler = FORECAST (chains=6)

# ASSEMBLE ALL COMPONENTS
from error import error
from datasets import datasets
from proposal import proposal
likelihood.assign (model, error)
replicates.assign (likelihood, datasets)
sampler.assign (replicates, proposal)
