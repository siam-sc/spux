from spux.models.randomwalk import Randomwalk
from exact import exact
from error import error

model = Randomwalk ()
parameters = exact ['parameters']
steps = 1000
period = 20
times = range (period, period + steps, period)
replicates = 3
inputsets = [None] * replicates

sandbox = None

from spux.utils.seed import Seed
seed = Seed (2)

# from spux.utils import synthesize
from spux.utils import synthesize
synthesize.generate (model, parameters, times, error, replicates, inputsets, sandbox = sandbox, seed = seed)