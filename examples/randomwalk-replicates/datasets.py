import os
import pandas
import glob

filenames = sorted (glob.glob (os.path.join ('datasets/', 'dataset_*.dat')))
columns = ['time','position']
datasets = {}
for name, filename in enumerate (filenames):
    datasets [name] = pandas.read_csv (filename, sep=",", usecols=columns, index_col=0)
