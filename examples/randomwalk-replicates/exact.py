# dictionary for exact parameters and predictions
exact = {}

# exact parameters
exact ['parameters'] = {}
exact ['parameters'] ['origin'] = 100
exact ['parameters'] ['drift'] = 0.2
exact ['parameters'] [r'$\sigma$'] = 10

# exact predictions
import os, pandas, glob
exact ['predictions'] = {}
filenames = sorted (glob.glob (os.path.join ('datasets/', 'predictions_*.dat')))
for name, filename in enumerate (filenames):
    if os.path.exists (filename):
        exact ['predictions'] [name] = pandas.read_csv (filename, sep=",", index_col=0)