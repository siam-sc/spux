# === load configuration and results

# import exact parameters
from exact import exact

# load posterior samples
from spux.io import loader
samples, infos = loader.reconstruct (burnin=0)

# === plot

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (samples, infos, exact=exact)

# plot unsuccessful posteriors
plot.unsuccessfuls ()

# plot resets of stuck chains
plot.resets ()

# compute and report approximated maximum a posterior (MAP) parameters estimate
plot.MAP ()

# plot samples
plot.parameters ()

# plot autocorrelations
plot.autocorrelations ()

# compute and report effective sample size (ESS)
plot.ESS ()

# plot marginal posterior distributions
plot.posteriors ()

# plot pairwise joint posterior distributions
plot.posteriors2d ()

# plot pairwise joint posterior distribution for selected parameter pairs
plot.posterior2d ('origin', 'drift')

# plot evolution of likelihoods
plot.likelihoods ()

# plot redraw rates
plot.redraw ()

# plot evolution of likelihood accuracies
plot.accuracies ()

# plot evolution of likelihood particles
plot.particles ()

# plot evolution of acceptances
plot.acceptances ()

# plot posterior model predictions including observations
plot.predictions ()

# plot quantile-quantile comparison of the error and residual distributions
plot.QQ ()

# show metrics
plot.metrics ()

# plot timestamps
plot.timestamps ()
keys = [ "evaluate", "routings", "wait", "init", "init sync", "run", "run sync", "errors", "errors sync", "resample", "resample sync"]
plot.timestamps (keys, suffix='-cherrypicked')

# generate report
from spux.report import generate
authors = r'Jonas {\v S}ukys'
generate.report (authors)