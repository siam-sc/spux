# === Randomwalk model

# construct Randomwalk model
from spux.models.randomwalk import Randomwalk
model = Randomwalk (stepsize = 1)

# === SPUX

# LIKELIHOOD
# for marginalization in stochastic models, construc a Particle Filter likelihood
from spux.likelihoods.pf import PF
likelihood = PF (particles=[4, 8])

# REPLICATES
# use Replicates likelihood to combine above likelihood with multiple datasets
from spux.likelihoods.replicates import Replicates
replicates = Replicates ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=4)

# ASSEMBLE ALL COMPONENTS
from error import error
from datasets import datasets
from prior import prior
likelihood.assign (model, error)
replicates.assign (likelihood, datasets)
sampler.assign (replicates, prior)