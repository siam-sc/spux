
# load posterior samples
from spux.io import loader
samples, infos = loader.reconstruct ()

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (samples, infos)

# plot traffic of the PF likelihood resampling
plot.traffic ()

# plot runtimes
keys = ["evaluate", "init", "init sync", "run", "run sync", "errors", "errors sync", "resample", "resample sync"]
plot.runtimes (keys)
plot.runtimes (suffix = '-all')

# plot efficiency
plot.efficiency ()

# plot timestamps
keys = [ "evaluate", "routings", "wait", "init", "init sync", "run", "run sync", "errors", "errors sync", "resample", "resample sync"]
plot.timestamps (sample = 0)
plot.timestamps (keys, sample = 0, suffix='-cherrypicked')