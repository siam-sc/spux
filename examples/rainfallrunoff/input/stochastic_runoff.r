## =======================================================
## Project: very simple stochastic hydrological model to test Spux
##
## March  8, 2019 -- Andreas Scheidegger
##
## andreas.scheidegger@eawag.ch
## =======================================================

## load (and try to install) package to solve ODE
if(!require(deSolve)){
    print("Attempt to install R package 'deSolve'.")
    install.packages("somepackage")
    library(somepackage)
}


## 1.) =================================
## We wrap the actual model in a function that returns a state-full
## version of the model.  Spux runs the model runs the model
## step wise. The user code has to manage all relevant states!
## 
## Arguments:
##  state0: list with the initial state at t=0
##
## Value: a stateful function to run the model stepwise
statefull.model <- function(state.init = list(RP=0, Vt0=c(V1=0, V2=0))){
    
    ## the states of the model are passed as closure    
    state <- state.init

    
    ## ----------------------
    ## Sample a trajectory (of some strange random process)
    ## at point in times 't' with memory given in state[min(t)].
    ##
    ## Arguments:
    ##  t:      vector of time points
    ##  state:  "memory" of the process at min(t)
    ##  seed:   seed for random generator
    ##
    ## Value:  list of tracjectory and state at max(t)j
    ##
    ## N.B. this is a very slow implementation!
    sample.trajectory <- function(t, state, seed){
        set.seed(seed)
        x <- rep(NA, length(t))
        x[1] <- state
        for(i in 2:length(t)){
            x[i] <- state <- abs( (0.6*state + sqrt(t[i] - t[i-1])*rnorm(1)) )
        }
        
        list(trajectory=x, state=state) 
    }


 
    ## ----------------------
    ## A very simple, stochastic two-bucket run-off model
    ##
    ## Arguments:
    ##  theta:              named parameter vector: c('a', 'b', 'c', 'Q.base')
    ##  times:              vector if points in time to evaluate the model
    ##  rain:               data.frame with columns 'time' and 'rain'
    ##  seed:               integer to set seed to define the random trajectory
    ##  method:             algorithm used by deSolve::ode
    ##
    ## Value:               vector of runoff at time points 'times'
    runoff.model.stochastic <- function(theta, t0, t1, rain,
                                        seed, method="lsoda") {


        ## --- stochastic input
        rain <- rain[rain$time > t0 | rain$time <= t1,]
        trac <- sample.trajectory(t=rain$time,
                                  state=state$RP, seed=seed)

        ## ! update state of random process
        state$RP <<- trac$state 

        ## add stochastic process to observed rain
        rain$rain <- rain$rain + trac$trajectory
        
        ## function to interpolate the rain at any time point. This is needed for the ODE solver
        rain.interpolator <- approxfun(rain$time, rain$rain + trac$trajectory, rule = 2)
        
        ## --- define model as ODE
        dV <- function(t, V, parms, rain.interpolator)  {
            with(as.list(c(parms, V)), {

                dV1 <- a*rain.interpolator(t) - b*V1
                dV2 <- b*V1 - c*V2 

                list(c(dV1, dV2))
            })
        }

        ## Solve ODE
        out <- ode(y = state$Vt0, times = c(t0, t1),
                   func = dV, parms = theta, 
                   rain.interpolator = rain.interpolator,
                   method = method)

        ## ! update state of ODE model
        state$Vt0 <<- out[nrow(out), 2:3]

        ## compute runoff at the outlet of the catchment
        Q <- theta['c']*out[2,3] + theta['Q.base']
 
        return(Q)
    }


    ## returns the funchtion
    return(runoff.model.stochastic)
}



## 2.) =================================
## load (simulate) external rain data
rain <- data.frame(time = seq(0, 200, 1),
                   rain = round(sin(seq(0, 200, 1)/15))+1)


## 3.) =================================
## initialize stateful model

runoff.mod <- statefull.model(state.init = list(RP=0,
                                                Vt0=c(V1=5, V2=3))
                              )

## --- You could run the model like this:
## runoff.mod(theta=c(a=0.2, b=0.1, c=0.3, Q.base=1),
##            t0=0, t1=3, seed=31415, rain=rain)




## 0.) =================================
## code used to generate teh data set

## Q <- rep(0, 85)
## times <- seq(0, 190, 2)

## for(i in 2:length(times)){
##     Q[i] <- runoff.mod(theta=c(a=0.2, b=0.1, c=0.3, Q.base=1),
##                        t0=times[i-1], t1=times[i], seed=314152, rain=rain)
## }

## write.table(data.frame(time=times, runoff=Q), file="obs.csv", sep=";", row.names=FALSE)
