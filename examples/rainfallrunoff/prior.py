## -------------------------------------------------------
##
## File: prior.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

# prior distributions using stats.scipy for each parameter independently
# available options (univariate): https://docs.scipy.org/doc/scipy/reference/stats.html

from scipy import stats


from spux.distributions.tensor import Tensor

distributions = {}

distributions [r'$\theta_a$'] = stats.uniform (loc=0, scale=1)
distributions [r'$\theta_b$'] = stats.uniform (loc=0, scale=1)
distributions [r'$\theta_c$'] = stats.uniform (loc=0, scale=1)
distributions [r'$\theta_{Q.base}$'] = stats.uniform (loc=0, scale=3)


distributions [r'$\sigma$'] = stats.lognorm (scale=0.05, s=0.2)

# construct a joint distribution for a vector of independent parameters by tensorization
prior = Tensor (distributions)
