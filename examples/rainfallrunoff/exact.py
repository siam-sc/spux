# dictionary for exact parameters and predictions
exact = {}

# exact parameters
exact ['parameters'] = {}
exact ['parameters'] [r'$\sigma$'] = 0.05
exact ['parameters'] [r'$\theta_a$'] = 0.2
exact ['parameters'] [r'$\theta_b$'] = 0.1
exact ['parameters'] [r'$\theta_c$'] = 0.3
exact ['parameters'] [r'$\theta_{Q.base}$'] = 1

# exact predictions
import os, pandas
filename = 'datasets/predictions.dat'
if os.path.exists (filename):
    exact ['predictions'] = pandas.read_csv (filename, sep=",", index_col=0)