# === CONNECTOR INITIALIZATION

from spux.executors.mpi4py.connectors import utils
connector = utils.select ('auto')

# === SAMPLER with attached executors

from script_executors import sampler

# === SAMPLING

# SANDBOX
from spux.utils.sandbox import Sandbox
sandbox = Sandbox (template = 'input')

# SEED
from spux.utils.seed import Seed
seed = Seed (8)

# init executor
sampler.executor.init (connector)

# setup sampler
sampler.setup (sandbox=sandbox, verbosity=3, index=0, seed=seed)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (15)

# exit executor
sampler.executor.exit ()