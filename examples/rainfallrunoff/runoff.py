## -------------------------------------------------------
##
## File: runoff.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

import os

from spux.models.model import Model
from spux.utils.annotate import annotate

# to interface R
import rpy2.robjects as r
from rpy2.robjects.vectors import FloatVector, StrVector

class Runoff (Model):

    # no need for sandboxing
    sandboxing = 1

    # construct model
    def __init__ (self, method="lsode"):
        self.method = method # ODE solver

    # initialize model using specified 'inputset' and 'parameters'
    def init (self, inputset, parameters):

        # base class 'init (...)' method - OPTIONAL
        Model.init (self, inputset, parameters)

        # load R file that defines the stateful R function 'runoff.mod'
        # and loads (simulates) the rainfall data.
        r.r('source("%s")' % os.path.join (self.sandbox(), 'stochastic_runoff.r'))

        # map R function to python function
        self.runoffmodel = r.r['runoff.mod'] #

        ## construct theta, as named R vector
        labels = [r'$\theta_a$', r'$\theta_b$', r'$\theta_c$', r'$\theta_{Q.base}$']
        theta = FloatVector([ parameters [label] for label in labels ] )
        theta.names = StrVector(['a', 'b', 'c', 'Q.base']) # must be a named vector!
        self.theta = theta

        self.time = 0           # initial time

    # run model up to specified 'time' and return the prediction
    def run (self, time):

        # run model in R
        Q = self.runoffmodel(theta = self.theta,
                             t0 = self.time,
                             t1 = time,
                             rain = r.r['rain'],
                             seed = int(self.seed()[-1]), # buffer would be better
                             method = self.method)

        # update time
        self.time = time

        # return results
        return annotate (list(Q), ['runoff'], time)
