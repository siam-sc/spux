## -------------------------------------------------------
##
## File: script.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

# === runoff model

# construct Runoff model
from runoff import Runoff
model = Runoff (method="lsode")

# === SPUX

# LIKELIHOOD
# construct likelihood - for marginalization in for stochastic models, use Particle Filter
from spux.likelihoods.pf import PF
likelihood = PF (particles=5)   # just for testing, better set to 32

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=5)      # just for testing, better set to 32

# ASSEMBLE ALL COMPONENTS
from error import error
from dataset import dataset
from prior import prior
likelihood.assign (model, error, dataset)
sampler.assign (likelihood, prior)