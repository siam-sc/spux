## -------------------------------------------------------
##
## File: dataset.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

import pandas

columns = ['time','runoff']
dataset = pandas.read_csv ('datasets/obs.csv', sep=";", usecols=columns, index_col=0)
