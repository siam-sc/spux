## -------------------------------------------------------
##
## File: error.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

from scipy import stats
from spux.distributions.tensor import Tensor

# define an error model
class Error (object):

    # return an error model (distribution) for the specified prediction and parameters
    def distribution (self, prediction, parameters):

        # specify error distributions or each observed variable independently
        distributions = {}
        distributions ['runoff'] = stats.norm (loc=prediction['runoff'],
                                               scale=parameters[r'$\sigma$'])

        # construct a joint distribution for a vector of independent parameters by tensorization
        distribution = Tensor (distributions)

        return distribution

error = Error ()
