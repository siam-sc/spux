# generate config
import script
del script

# import exact parameters
from exact import exact

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (exact = exact)

# plot dataset
plot.dataset ()

# plot marginal prior distributions
plot.priors ()

# plot error model distribution treating each
# dataset point as prediction and using exact parameters
plot.errors ()

# generate report
from spux.report import generate
authors = r'Andreas Scheidegger'
generate.report (authors)