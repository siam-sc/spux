## -------------------------------------------------------
##
## File: script_serial.py
##
## March  5, 2019 -- Andreas Scheidegger
## andreas.scheidegger@eawag.ch
## -------------------------------------------------------

# === SCRIPT

from script import sampler

# === SAMPLING

# SEED
from spux.utils.seed import Seed
seed = Seed (8)

# SANDBOX
from spux.utils.sandbox import Sandbox
sandbox = Sandbox (template = 'input')

# init executor
sampler.executor.init ()

# setup sampler
sampler.setup (sandbox=sandbox, verbosity=9, index=0, seed=seed)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (15)

# exit executor
sampler.executor.exit ()
