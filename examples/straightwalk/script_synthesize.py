from spux.models.straightwalk import Straightwalk
from exact import exact
from error import error

model = Straightwalk ()
parameters = exact ['parameters']
steps = 1000
period = 20
times = range (period, period + steps, period)
seed = 2

# from spux.utils import synthesize
from spux.utils import synthesize
synthesize.generate (model, parameters, times, error, seed=seed)