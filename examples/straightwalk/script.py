# === Straightwalk model

# construct Straightwalk model
from spux.models.straightwalk import Straightwalk
model = Straightwalk (stepsize = 1)

# === SPUX

# LIKELIHOOD
# construct direct likelihood (will use error model directly)
from spux.likelihoods.direct import Direct
likelihood = Direct ()

# SAMPLER
# construct EMCEE sampler
from spux.samplers.emcee import EMCEE
sampler = EMCEE (chains=4)

# ASSEMBLE ALL COMPONENTS
from error import error
from dataset import dataset
from prior import prior
likelihood.assign (model, error, dataset)
sampler.assign (likelihood, prior)