# === SCRIPT with SPUX components

from script import sampler

# === EXECUTORS

# import required executor classes
from spux.executors.mpi4py.pool import Mpi4pyPool

# create executors with the specified number of parallel workers and attach them
sampler.attach (Mpi4pyPool (workers=2))

# display resources table
sampler.executor.table ()