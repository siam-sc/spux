# === SCRIPT

from script import sampler

# === SAMPLING

# SANDBOX
# for compute clusters, set "target" path to scratch file system -
# then a symlink named "path" will be created instead
# alternatively, set "path" to None and "target" to local scratch or "/dev/shm/spux-sandbox" to use fast tmpfs
from spux.utils.sandbox import Sandbox
sandbox = Sandbox (path = None, target = '/dev/shm/spux-sandbox')

# SEED
from spux.utils.seed import Seed
seed = Seed (8)

# init executor
sampler.executor.init ()

# setup sampler
sampler.setup (sandbox = sandbox, verbosity = 1, index = 0, seed = seed)

# init sampler (use prior for generating starting values)
sampler.init ()

# generate samples from posterior distribution
sampler (12)

# exit executor
sampler.executor.exit ()
