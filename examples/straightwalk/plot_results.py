# === load configuration and results

# import exact parameters
from exact import exact

# load posterior samples
from spux.io import loader
samples, infos = loader.reconstruct ()

# burnin sample batch
burnin = 0

# === plot

# plotting class
from spux.plot.mpl import MatPlotLib
plot = MatPlotLib (samples, infos, burnin = burnin, exact = exact)

# plot unsuccessful posteriors
plot.unsuccessfuls ()

# plot resets of stuck chains
plot.resets ()

# compute and report maximum a posterior (MAP) estimate of parameters and the associated likelihood
plot.MAP ()

# plot samples
plot.parameters ()

# plot evolution of likelihoods
plot.likelihoods ()

# plot evolution of acceptances
plot.acceptances ()

# plot timestamps
plot.timestamps ()
keys = [ "evaluate", "routings", "wait", "init", "init sync", "run", "run sync", "errors", "errors sync", "resample", "resample sync"]
plot.timestamps (keys, suffix='-cherrypicked')

# === remove burnin

samples, infos = loader.tail (samples, infos, batch = burnin)
plot = MatPlotLib (samples, infos, burnin = burnin, tail = burnin, exact = exact)
plot.MAP ()

# plot autocorrelations
plot.autocorrelations ()

# compute and report effective sample size (ESS)
plot.ESS ()

# plot marginal posterior distributions
plot.posteriors ()

# plot pairwise joint posterior distributions
plot.posteriors2d ()

# plot pairwise joint posterior distribution for selected parameter pairs
plot.posterior2d ('origin', 'drift')

# plot posterior model predictions including observations
plot.predictions ()

# plot quantile-quantile comparison of the error and residual distributions
plot.QQ ()

# show metrics
plot.metrics ()

# generate report
from spux.report import generate
authors = r'Jonas {\v S}ukys'
generate.report (authors)