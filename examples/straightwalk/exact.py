
# dictionary for exact parameters and predictions
exact = {}

# exact parameters
exact ['parameters'] = {}
exact ['parameters'] ['origin'] = 100
exact ['parameters'] ['drift'] = 0.2
exact ['parameters'] [r'$\sigma$'] = 10

# exact predictions
import os, pandas
filename = 'datasets/predictions.dat'
if os.path.exists (filename):
    exact ['predictions'] = pandas.read_csv (filename, sep=",", index_col=0)