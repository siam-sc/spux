#!/bin/bash
GIT_REPO_CI_BRANCH=$1

module purge
module load new

module load python
module load java
module load open_mpi/3.0.0
module list

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$LD_RUN_PATH

GIT_REPO_REMOTE=https://gitlab.com/siam-sc/spux.git

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# create TMP folder in /cluster/scratch ===============================

TMP=/cluster/scratch/${USER}/spux-$(date +%Y%m%d_%H%M)-$(uuidgen -r)
mkdir -p ${TMP}

VENV=${TMP}/venv3

# clone latest version from spux ======================================

GIT_REPO_LOCAL=${TMP}/spux

git clone --depth 1 ${GIT_REPO_REMOTE} -b ${GIT_REPO_CI_BRANCH} ${GIT_REPO_LOCAL}

# install JDK if not done already ====================================

URL=https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz
JDK_FOLDER=/cluster/scratch/${USER}/jdk
if test ! -d ${JDK_FOLDER}; then
    mkdir -p ${JDK_FOLDER}
    ls -l ${JDK_FOLDER}
    curl ${URL} | tar xz -C ${JDK_FOLDER}
else
    echo JDK found
fi

JDK_DIRNAME=$(ls ${JDK_FOLDER})
JAVA_HOME=${JDK_FOLDER}/${JDK_DIRNAME}

# create virtuale env =================================================

python3 -m venv $VENV
source $VENV/bin/activate

# install Python packages and spux ====================================

pip install --upgrade pip setuptools
pip install numpy

cd ${GIT_REPO_LOCAL}

pip install -r requirements_dev.txt
pip install -e .

python -c "import spux; print(spux.__file__)"


# other setup =========================================================

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:{$LD_RUN_PATH}

unset _JAVA_OPTIONS


# run test script with bsub ==========================================

echo "running examples/randomwalk-replicates in temporary directory:"
echo ${TMP}

OUT_FILE=${TMP}/report.dat
N_WORKERS=21

cd examples/randomwalk-replicates
bsub -n ${N_WORKERS} \
    -oo $OUT_FILE \
    -J spux-ci-rw \
    -K \
    -W 04:00 \
    mpirun -n ${N_WORKERS} --mca mpi_warn_on_fork 0 $VENV/bin/python3 -m mpi4py script_parallel.py
    #mpirun -n 1 --mca mpi_warn_on_fork 0 $VENV/bin/python3 -m mpi4py script_parallel.py
cd ../..

echo
echo -------------------------------------------
echo

cat ${OUT_FILE}

# check for errors ==================================================
# Note: the MABORT MPI error is specific to tests command
# (`mpirun ... python3 -m mpi4py`), hence, checked here, and not on the trigger side
#MPI_ABORT_INVOKED=$(cat ${OUT_FILE} | grep "MPI_ABORT was invoked")
#if [ ! -z "MPI_ABORT_INVOKED" ]; then
#    exit 1
#fi
