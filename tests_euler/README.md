# Tests on Euler cluster

## New Euler test user setup

Prerequisites: NETHZ account and network access to euler.ethz.ch

Cleanup previous setup and init new account:

    $ rm -r id_rsa* user_on_euler
    $ ./init_account.sh

Test manually (note: it will take few minutes):

    $ GIT_REPO_CI_BRANCH=$(git branch | grep \* | cut -d ' ' -f2) EULER_USER=$(cat ./user_on_euler) SSH_PRV_KEY=$(cat id_rsa) ./trigger_tests.sh

Save new user setup by updating `EULER_TESTS_USER` and `EULER_TESTS_SSH_PRV_KEY`
environment variables in [spux CI/CD
settings](https://gitlab.com/siam-sc/spux/settings/ci_cd) to contents of, respectively,
`./user_on_euler` and `./id_rsa` files.

## Files

- `./init_account.sh`

  - setup ssh key pair in this folder
  - scp `tests.sh` to `~/.spux/tests.sh` on euler
  - modify `.ssh/authorized_keys` on euler to enable password-less triggering of tests

    ! run this once (locally but with access to euler) and setup CI/CD vars w/ user name
    and created private key !

    Note: private key is only used for running tests, and does not allow login.


- `./trigger_tests.sh`

  - runs on test server
  - makes password-less connection and automatically triggers `.spux/tests.sh`
    (as configured in `.ssh/authorized_keys` on euler)
    - branch to checkout from git is read from `GIT_REPO_CI_BRANCH` env variable and
      passed to the `run_tests.sh` script as a parameter
  - captures server stdout to `result_of_testrun.txt` file
  - TODO: some output analysis could go here

- `./tests.sh`

  - confirm installation, xor
  - update `run_tests.sh`, xor
  - run `run_tests.sh`

- `./run_tests.sh`

  - create temp folder in `/cluster/scratch/$USER`
  - `git clone` of latest version of a given spux branch which name is given as the
    script parameter
  - installs JDK if needed
  - created python virtualenv
  - installs python packages
  - runs a test script with bsub -K (runs job and waits till the job finishes) and
    `-m mpi4py` Python module hook (to shutdown MPI in case of errors, else euler
    job will hang)
  - prints job output to stdout
  - checks job output for MPI_ABORT and exits with error code if found
  - TODO: some more output analysis could go here
