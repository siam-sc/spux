if ! test -f id_rsa; then
    ssh-keygen -f id_rsa -P ""
    echo
    echo "created key pair"
    echo
fi

echo -n "login name on euler ? "
read EULER_USER

echo ${EULER_USER} > user_on_euler
HOST=euler.ethz.ch

function check_account_setup
{
    # in case the runner script is already installed it will output a message
    # "tests.sh installed" and exit when started with an "check" cmdline argument.
    #
    # cases:
    #
    # 1. key is not installed: -o PasswordAuthentication=No will cause failure.
    #
    # 2. there is already key, but no runner script: will most likely give "file not
    #    found" error, but not the expected pattern "tests.sh installed", thus exit code
    #    will also not be 0
    #
    # in all other cases the runner script exists already

    ssh -o PasswordAuthentication=No -i id_rsa ${EULER_USER}@${HOST} check 2>/dev/null | grep "tests.sh installed" >/dev/null
}

function setup_account
{
    # open channel / multiplexing
    ARGS="-o ControlPath=~/.ssh/ctrl-%r-%h-%p ${EULER_USER}@${HOST}"

    ssh -f -o ControlMaster=yes ${ARGS} sleep 9999 2>/dev/null >&2

    ssh ${ARGS} mkdir -p .spux_ci  2>/dev/null >&2
    sed -i 's/\r//' ./tests.sh
    scp ./tests.sh ${ARGS}:.spux_ci  2>/dev/null >&2

    PUBLIC_KEY=$(cat id_rsa.pub)
    COMMAND=".spux_ci/tests.sh \${SSH_ORIGINAL_COMMAND}"

    echo command=\"${COMMAND}\" ${PUBLIC_KEY} | ssh ${ARGS} "cat >> .ssh/authorized_keys" 2>/dev/null 1>&2

    # close channel / stop multiplexing
    ssh -O exit ${ARGS} 2>/dev/null >&2
}

# check if account already setup
if check_account_setup; then
    echo
    echo account already setup
    exit
fi

setup_account;

if check_account_setup; then
    echo
    echo account setup succeeded
    echo
    echo you must set GitLab CI/CD variables:
    echo
    echo    - EULER_TESTS_SSH_PRV_KEY to content of \"id_rsa\" file
    echo    - EULER_TESTS_USER to content of \"user_on_euler\" file
    echo
    echo to make ci tests on euler work
else
    ssh -o PasswordAuthentication=No -i id_rsa ${EULER_USER}@${HOST} ls
    echo
    echo account setup failed !!!!
fi
