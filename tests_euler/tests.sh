#!/bin/bash

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
cd ${HERE}

if [ "$1" = "check" ]; then

    echo "tests.sh installed"

elif [ "$1" = "update" ]; then

    if [ -f run_tests.sh ]; then
        rm run_tests.sh
    fi
    while read line
    do
        echo "$line" >> run_tests.sh
    done </dev/stdin
    chmod a+x run_tests.sh

elif [ "$1" = "run" ]; then

    if [ ! -f run_tests.sh ]; then
        echo "ERROR: update run script first"
    fi
    ./run_tests.sh "${@:2}"

else

    echo "ERROR: command not recognized"
    exit 1

fi
