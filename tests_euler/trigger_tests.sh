#!/bin/bash

# this script requires that ./init_account.sh was successfully
# executed before.

abort() {
    msg=$1
    echo
    echo -n "ABORT"
    if [ -z "${msg}" ]; then
        echo
    else
        echo ": ${msg}"
    fi
    exit 1
}
trap 'abort' ERR

HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd ${HERE}

HOST=euler.ethz.ch

echo -n "Current git branch.. "
if [ -z "${GIT_REPO_CI_BRANCH}" ]; then
    abort "GIT_REPO_CI_BRANCH variable empty"
fi
echo "${GIT_REPO_CI_BRANCH}"

echo -n "Euler user.. "
if [ -z "${EULER_USER}" ]; then
    abort "EULER_USER variable empty"
fi
echo "${EULER_USER}@${HOST}"

echo -n "Setup SSH key.. "
if [ -z "${SSH_PRV_KEY}" ]; then
    abort "SSH_PRV_KEY variable empty"
fi
# sed - see https://gitlab.com/gitlab-org/gitlab-ce/issues/3720
echo "${SSH_PRV_KEY}" | sed 's/\r /\n/g' > ./id_rsa
# https://stackoverflow.com/questions/59895
chmod go-rwx id_rsa
echo "OK"

echo -n "Update run script on Euler.. "
cat ./run_tests.sh | ssh -o StrictHostKeyChecking=no -i id_rsa ${EULER_USER}@${HOST} update -
echo "OK"

echo
echo -n "Dispatch to Euler.."
ssh -o StrictHostKeyChecking=no -t -i id_rsa ${EULER_USER}@${HOST} run ${GIT_REPO_CI_BRANCH} | tee report.dat
SSH_EXIT_CODE=${PIPESTATUS[0]}
echo "DONE"

echo
echo " === REPORT:"
cat report.dat

echo
echo -n "SSH_EXIT_CODE: "
echo ${SSH_EXIT_CODE}

if [ $SSH_EXIT_CODE -gt 0 ]; then
    if [ ${SSH_EXIT_CODE} = 255 ]; then
        msg="SSH connection error"
    else
        msg="Euler test script error"
    fi
    abort "${msg}"
fi

SUCCESS=$(cat report.dat | grep -c "Successfully completed.")
if [ $SUCCESS -eq 0 ]; then
    echo
    echo "JOB FAILED"
    exit 1
else
    echo
    echo "JOB SUCCESSFUL"
fi

echo
echo "TESTING DONE"

# todo: analyse result_of_testrun.txt
