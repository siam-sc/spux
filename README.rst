===================================
Scalable Uncertainty Quantification
===================================

.. image:: https://img.shields.io/pypi/v/spux.svg
        :target: https://pypi.python.org/pypi/spux

.. image:: https://readthedocs.org/projects/spux/badge/?version=stable
        :target: https://spux.readthedocs.io/en/latest/?badge=stable
        :alt: Documentation status

.. image:: https://gitlab.com/siam-sc/spux/badges/master/pipeline.svg
        :target: https://gitlab.com/siam-sc/spux/commits/master
        :alt: Pipeline status

.. image:: https://gitlab.com/siam-sc/spux/badges/master/coverage.svg
        :target: https://gitlab.com/siam-sc/spux/commits/master
        :alt: Coverage report

| SPUX - "Scalable Package for Uncertainty Quantification".
|
| SPUX is a modular framework for Bayesian inference and uncertainty quantification.
| SPUX can be coupled with linear and nonlinear, deterministic and stochastic models.
| SPUX supports model in any programming language (e.g. Python, R, Julia, C/C++, Fortran, Java).
| SPUX scales effortlessly from serial run to parallel high performance computing clusters.
| SPUX is application agnostic, with current examples in environmental dataset sciences.
|
| SPUX is actively developed at Eawag, Swiss Federal Institute of Aquatic Science and Technology,
| by researchers in the High Performance Scientific Computing Group, https://www.eawag.ch/sc.
| For the scientific website of the SPUX project, please refer to https://eawag.ch/spux.
|
| Documentation is available at https://spux.readthedocs.io.
| Source code repository is available at https://gitlab.com/siam-sc/spux.
|
| You are welcome to browse through the results gallery of the models already coupled to spux at https://spux.readthedocs.io/en/stable/gallery.html.
|
| This is free software, distributed under Apache (v2) License.
|
| If you use this software, please cite (preprint available at http://arxiv.org/abs/1711.01410):

.. code::

        Šukys, J. and Kattwinkel, M.
        "SPUX: Scalable Particle Markov Chain Monte Carlo
        for uncertainty quantification in stochastic ecological models".
        Advances in Parallel Computing - Parallel Computing is Everywhere,
        IOS Press, (32), pp. 159–168, 2018.


