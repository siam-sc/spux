import pytest
import os

from examples_utils import run_example_script

@pytest.mark.long
@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["hydro"], indirect=True)
def test_hydro(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("plot_config.py")
    # assert os.path.exists("./fig/hydro_distributions-prior.png")
    # assert os.path.exists("./fig/hydro_datasets-x.png")
    # assert os.path.exists("./fig/hydro_datasets-y.png")
    # assert os.path.exists("./fig/hydro_errors-dataset-1.png")
    # assert os.path.exists("./fig/hydro_errors-dataset-2.png")
    # assert os.path.exists("./fig/hydro_errors-dataset-3.png")
    # assert os.path.exists("./fig/hydro_distributions-error.png")

    run_example_script("script_serial.py")
    run_example_script("plot_results.py")
    # assert os.path.exists("./fig/hydro_parameters.png")
    # assert os.path.exists("./fig/hydro_posteriors.png")
    # assert os.path.exists("./fig/hydro_posteriors2d.png")
    # assert os.path.exists("./fig/hydro_likelihoods.png")
    # assert os.path.exists("./fig/hydro_redraw.png")
    # assert os.path.exists("./fig/hydro_deviations.png")
    # assert os.path.exists("./fig/hydro_acceptances.png")
    # assert os.path.exists("./fig/hydro_autocorrelations.png")
    # assert os.path.exists("./fig/hydro_predictions-posterior-dataset-1.png")
    # assert os.path.exists("./fig/hydro_predictions-posterior-dataset-2.png")
    # assert os.path.exists("./fig/hydro_predictions-posterior-dataset-3.png")
    # assert os.path.exists("./fig/hydro_qq.png")

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.skip (reason="Passing locally, failing on runners due to dict.keys() pickling problem.")
@pytest.mark.long
@pytest.mark.integration
def test_hydro_profile(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("script_serial_profile.py")
    run_example_script("plot_profiler.py")
    assert os.path.exists("./fig/hydro_profile.pstats")
    assert os.path.exists("./fig/hydro_profile.txt")
    assert os.path.exists("./fig/hydro_profile.png")
