import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.integration
@pytest.mark.long
@pytest.mark.parametrize("example_setup_and_teardown", ["rainfallrunoff"], indirect=True)
def test_rainfallrunoff(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("plot_config.py")
    # assert os.path.exists("./fig/rainfallrunoff_distributions-prior.png")
    # assert os.path.exists("./fig/rainfallrunoff_datasets-runoff.png")
    # assert os.path.exists("./fig/rainfallrunoff_errors-dataset-data.png")

    run_example_script("script_serial.py")
    run_example_script("script_serial_continue.py")
    run_example_script("plot_results.py")
    # assert os.path.exists("./fig/rainfallrunoff_parameters.png")
    # assert os.path.exists("./fig/rainfallrunoff_posteriors.png")
    # assert os.path.exists("./fig/rainfallrunoff_posteriors2d.png")
    # assert os.path.exists("./fig/rainfallrunoff_likelihoods.png")
    # assert os.path.exists("./fig/rainfallrunoff_redraw.png")
    # assert os.path.exists("./fig/rainfallrunoff_deviations.png")
    # assert os.path.exists("./fig/rainfallrunoff_acceptances.png")
    # assert os.path.exists("./fig/rainfallrunoff_autocorrelations.png")
    # assert os.path.exists("./fig/rainfallrunoff_predictions-posterior-dataset-data.png")
    # assert os.path.exists("./fig/rainfallrunoff_qq.png")
