#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import sys # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk-replicates"], indirect=True)
def test_randomwalk_replicates (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("plot_config.py")
    # assert os.path.exists ('./fig/randomwalk_distributions-prior.png')
    # assert os.path.exists ('./fig/randomwalk_dataset.png')
    # assert os.path.exists ('./fig/randomwalk_errors.png')

    run_example_script("script_serial.py")
    # assert os.path.exists ('./report/randomwalk_environment.txt')
    # assert os.path.exists ('./report/randomwalk_environment.dat')
    # assert os.path.exists ('./report/randomwalk_config.txt')
    # assert os.path.exists ('./report/randomwalk_config.dat')

    run_example_script("plot_results.py")
    # assert os.path.exists ('./report/randomwalk_MAP.txt')
    # assert os.path.exists ('./report/randomwalk_MAP.dat')
    # assert os.path.exists ('./report/randomwalk_ESS.dat')
    # assert os.path.exists ('./report/randomwalk_ESS.txt')
    # assert os.path.exists ('./fig/randomwalk_parameters.png')
    # assert os.path.exists ('./fig/randomwalk_posteriors.png')
    # assert os.path.exists ('./fig/randomwalk_posteriors2d.png')
    # assert os.path.exists ('./fig/randomwalk_likelihoods.png')
    # assert os.path.exists ('./fig/randomwalk_redraw.png')
    # assert os.path.exists ('./fig/randomwalk_accuracies.png')
    # assert os.path.exists ('./fig/randomwalk_acceptances.png')
    # assert os.path.exists ('./fig/randomwalk_autocorrelations.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-0.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-1.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-2.png')
    # assert os.path.exists ('./fig/randomwalk_qq.png')