import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk"], indirect=True)
def test_randomwalk(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("plot_config.py")
    # assert os.path.exists("./fig/randomwalk_distributions-prior.png")
    # assert os.path.exists("./fig/randomwalk_datasets-position.png")
    # assert os.path.exists("./fig/randomwalk_errors-dataset-0.png")
    # assert os.path.exists("./fig/randomwalk_errors-dataset-1.png")
    # assert os.path.exists("./fig/randomwalk_errors-dataset-2.png")

    run_example_script("script_serial.py")
    run_example_script("script_serial_continue.py")
    run_example_script("plot_results.py")
    # assert os.path.exists("./fig/randomwalk_parameters.png")
    # assert os.path.exists("./fig/randomwalk_posteriors.png")
    # assert os.path.exists("./fig/randomwalk_posteriors2d.png")
    # assert os.path.exists("./fig/randomwalk_likelihoods.png")
    # assert os.path.exists("./fig/randomwalk_redraw.png")
    # assert os.path.exists("./fig/randomwalk_deviations.png")
    # assert os.path.exists("./fig/randomwalk_acceptances.png")
    # assert os.path.exists("./fig/randomwalk_autocorrelations.png")
    # assert os.path.exists("./fig/randomwalk_predictions-posterior-dataset-0.png")
    # assert os.path.exists("./fig/randomwalk_predictions-posterior-dataset-1.png")
    # assert os.path.exists("./fig/randomwalk_predictions-posterior-dataset-2.png")
    # assert os.path.exists("./fig/randomwalk_qq.png")

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.long
@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk"], indirect=True)
def test_randomwalk_profile(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("script_serial_profile.py")
    run_example_script("plot_profiler.py")
    # assert os.path.exists("./fig/randomwalk_profile.pstats")
    # assert os.path.exists("./fig/randomwalk_profile.txt")
    # assert os.path.exists("./fig/randomwalk_profile.png")

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.long
@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk"], indirect=True)
def test_randomwalk_informative(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("script_serial_informative.py")
    run_example_script("plot_performance.py")
    # assert os.path.exists("./fig/randomwalk_traffic.png")
    # assert os.path.exists("./fig/randomwalk_runtimes.png")
    # assert os.path.exists("./fig/randomwalk_runtimes-all.png")
    # assert os.path.exists("./fig/randomwalk_efficiency.png")
    # assert os.path.exists("./fig/randomwalk_timestamps-S00000-R-0.png")
    # assert os.path.exists("./fig/randomwalk_timestamps-S00000-R-0-all.png")
