import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.integration
@pytest.mark.parametrize("example_setup_and_teardown", ["straightwalk"], indirect=True)
def test_straightwalk(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script("plot_config.py")
    # assert os.path.exists("./fig/straightwalk_distributions-prior.png")
    # assert os.path.exists("./fig/straightwalk_datasets-position.png")
    # assert os.path.exists("./fig/straightwalk_errors-dataset-0.png")
    # assert os.path.exists("./fig/straightwalk_errors-dataset-1.png")
    # assert os.path.exists("./fig/straightwalk_errors-dataset-2.png")
    # assert os.path.exists("./fig/straightwalk_distributions-error.png")

    run_example_script("script_serial.py")
    run_example_script("script_serial_continue.py")
    run_example_script("plot_results.py")
    # assert os.path.exists("./fig/straightwalk_parameters.png")
    # assert os.path.exists("./fig/straightwalk_posteriors.png")
    # assert os.path.exists("./fig/straightwalk_posteriors2d.png")
    # assert os.path.exists("./fig/straightwalk_likelihoods.png")
    # assert os.path.exists("./fig/straightwalk_acceptances.png")
    # assert os.path.exists("./fig/straightwalk_autocorrelations.png")
    # assert os.path.exists("./fig/straightwalk_predictions-posterior-dataset-0.png")
    # assert os.path.exists("./fig/straightwalk_predictions-posterior-dataset-1.png")
    # assert os.path.exists("./fig/straightwalk_predictions-posterior-dataset-2.png")
    # assert os.path.exists("./fig/straightwalk_qq.png")