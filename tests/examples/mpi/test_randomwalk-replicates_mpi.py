#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.

@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk-replicates"], indirect=True)
def test_randomwalk_replicates_parallel (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("plot_config")
    # assert os.path.exists ('./fig/randomwalk_distributions-prior.png')
    # assert os.path.exists ('./fig/randomwalk_datasets-position.png')
    # assert os.path.exists ('./fig/randomwalk_errors-dataset-0.png')
    # assert os.path.exists ('./fig/randomwalk_errors-dataset-1.png')
    # assert os.path.exists ('./fig/randomwalk_errors-dataset-2.png')

    run_example_script ("script_parallel")
    # assert os.path.exists ('./report/randomwalk_environment.txt')
    # assert os.path.exists ('./report/randomwalk_environment.dat')
    # assert os.path.exists ('./report/randomwalk_config.txt')
    # assert os.path.exists ('./report/randomwalk_config.dat')
    # assert os.path.exists ('./report/randomwalk_resources.txt')
    # assert os.path.exists ('./report/randomwalk_resources.dat')

    run_example_script ("plot_results")
    # assert os.path.exists ('./report/randomwalk_MAP.txt')
    # assert os.path.exists ('./report/randomwalk_MAP.dat')
    # assert os.path.exists ('./report/randomwalk_ESS.dat')
    # assert os.path.exists ('./report/randomwalk_ESS.txt')
    # assert os.path.exists ('./fig/randomwalk_parameters.png')
    # assert os.path.exists ('./fig/randomwalk_posteriors.png')
    # assert os.path.exists ('./fig/randomwalk_posteriors2d.png')
    # assert os.path.exists ('./fig/randomwalk_likelihoods.png')
    # assert os.path.exists ('./fig/randomwalk_redraw.png')
    # assert os.path.exists ('./fig/randomwalk_accuracies.png')
    # assert os.path.exists ('./fig/randomwalk_acceptances.png')
    # assert os.path.exists ('./fig/randomwalk_autocorrelations.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-0.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-1.png')
    # assert os.path.exists ('./fig/randomwalk_predictions-posterior-dataset-2.png')
    # assert os.path.exists ('./fig/randomwalk_qq.png')

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.long
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["randomwalk-replicates"], indirect=True)
def test_randomwalk_replicates_parallel_informative (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("script_parallel_informative")
    # assert os.path.exists ('./report/randomwalk_environment.txt')
    # assert os.path.exists ('./report/randomwalk_environment.dat')
    # assert os.path.exists ('./report/randomwalk_config.dat')
    # assert os.path.exists ('./report/randomwalk_config.txt')
    # assert os.path.exists ('./report/randomwalk_resources.txt')
    # assert os.path.exists ('./report/randomwalk_resources.dat')

    run_example_script ("plot_performance")

    # assert os.path.exists ('./fig/randomwalk_traffic.png')
    # assert os.path.exists ('./fig/randomwalk_runtimes.png')
    # assert os.path.exists ('./fig/randomwalk_runtimes-all.png')
    # assert os.path.exists ('./fig/randomwalk_efficiency.png')
    # assert os.path.exists ('./fig/randomwalk_timestamps-S00000-R-0.png')
    # assert os.path.exists ('./fig/randomwalk_timestamps-S00000-R-0-all.png')
