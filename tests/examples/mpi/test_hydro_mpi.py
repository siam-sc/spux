#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.

@pytest.mark.long
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["hydro"], indirect=True)
def test_hydro_parallel (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("plot_config")
    # assert os.path.exists ('./fig/hydro_distributions-prior.png')
    # assert os.path.exists ('./fig/hydro_datasets-x.png')
    # assert os.path.exists ('./fig/hydro_datasets-y.png')
    # assert os.path.exists ('./fig/hydro_errors-dataset-1.png')
    # assert os.path.exists ('./fig/hydro_errors-dataset-2.png')
    # assert os.path.exists ('./fig/hydro_errors-dataset-3.png')
    # assert os.path.exists ('./fig/hydro_distributions-error.png')

    run_example_script ("script_parallel")
    # assert os.path.exists ('./report/hydro_environment.txt')
    # assert os.path.exists ('./report/hydro_environment.dat')
    # assert os.path.exists ('./report/hydro_config.txt')
    # assert os.path.exists ('./report/hydro_config.dat')
    # assert os.path.exists ('./report/hydro_resources.txt')
    # assert os.path.exists ('./report/hydro_resources.dat')

    run_example_script ("plot_results")
    # assert os.path.exists ('./report/hydro_MAP.txt')
    # assert os.path.exists ('./report/hydro_MAP.dat')
    # assert os.path.exists ('./report/hydro_ESS.dat')
    # assert os.path.exists ('./report/hydro_ESS.txt')
    # assert os.path.exists ('./fig/hydro_parameters.png')
    # assert os.path.exists ('./fig/hydro_posteriors.png')
    # assert os.path.exists ('./fig/hydro_posteriors2d.png')
    # assert os.path.exists ('./fig/hydro_likelihoods.png')
    # assert os.path.exists ('./fig/hydro_redraw.png')
    # assert os.path.exists ('./fig/hydro_accuracies.png')
    # assert os.path.exists ('./fig/hydro_acceptances.png')
    # assert os.path.exists ('./fig/hydro_autocorrelations.png')
    # assert os.path.exists ('./fig/hydro_predictions-posterior-dataset-1.png')
    # assert os.path.exists ('./fig/hydro_predictions-posterior-dataset-2.png')
    # assert os.path.exists ('./fig/hydro_predictions-posterior-dataset-3.png')
    # assert os.path.exists ('./fig/hydro_qq.png')