#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.]

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["IBM_2species"], indirect=True)
def test_ibm2_parallel (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("plot_config.py")
    # assert os.path.exists ('./fig/IBM_2species_datasets-prey.pdf')
    # assert os.path.exists ('./fig/IBM_2species_datasets-predator.pdf')
    # assert os.path.exists ('./fig/IBM_2species_distributions-prior.pdf')
    # assert os.path.exists ('./fig/IBM_2species_errors-dataset-1.pdf')
    # assert os.path.exists ('./fig/IBM_2species_errors-dataset-2.pdf')
    # assert os.path.exists ('./fig/IBM_2species_errors-dataset-3.pdf')
    # assert os.path.exists ('./fig/IBM_2species_distributions-error.pdf')

    run_example_script ("script_parallel_emcee.py")
    run_example_script ("plot_results")
    # assert os.path.exists ('./fig/IBM_2species_unsuccessfuls.pdf')
    # assert os.path.exists ('./fig/IBM_2species_parameters.pdf')
    # assert os.path.exists ('./fig/IBM_2species_posteriors.pdf')
    # assert os.path.exists ('./fig/IBM_2species_posteriors2d.pdf')
    # assert os.path.exists ('./fig/IBM_2species_likelihoods.pdf')
    # assert os.path.exists ('./fig/IBM_2species_redraw.pdf')
    # assert os.path.exists ('./fig/IBM_2species_accuracies.pdf')
    # assert os.path.exists ('./fig/IBM_2species_acceptances.pdf')
    # assert os.path.exists ('./fig/IBM_2species_autocorrelations.pdf')
    # assert os.path.exists ('./fig/IBM_2species_predictions-posterior-dataset-1.pdf')
    # assert os.path.exists ('./fig/IBM_2species_predictions-posterior-dataset-2.pdf')
    # assert os.path.exists ('./fig/IBM_2species_predictions-posterior-dataset-3.pdf')
    # assert os.path.exists ('./fig/IBM_2species_qq.pdf')