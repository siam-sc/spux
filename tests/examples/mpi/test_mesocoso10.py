import pytest
import os

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.long
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["mesocoso10D"], indirect=True)
def test_ibm10_spawn(example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    print("# importing plot_config #")
    run_example_script("plot_config.py")
    print("#########")

    print("# pwd & ls #")
    os.system("pwd")
    os.system("ls")
    print("#########")

    # assert os.path.exists("./fig/mesocoso10D_datasets-asellaqua.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-chironomi.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-dugesia.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-ephaem.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-gastro.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-hetero.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-odonat.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-oligochet.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-simulati.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-tricho.pdf")

    print("# run plot_config from command line #")
    os.system("python3 plot_config.py")
    print("#########")

    print("# pwd & ls #")
    os.system("pwd")
    os.system("ls")
    print("#########")

    # assert os.path.exists("./fig/mesocoso10D_datasets-asellaqua.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-chironomi.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-dugesia.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-ephaem.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-gastro.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-hetero.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-odonat.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-oligochet.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-simulati.pdf")
    # assert os.path.exists("./fig/mesocoso10D_datasets-tricho.pdf")

    run_example_script("script_parallel_short.py")
    run_example_script("plot_results_short.py")
    # assert os.path.exists("./fig/mesocoso10D_unsuccessfuls.pdf")
    # assert os.path.exists("./fig/mesocoso10D_parameters.pdf")
    # assert os.path.exists("./fig/mesocoso10D_posteriors.pdf")
    # assert os.path.exists(
    #     "./fig/mesocoso10D_posterior2d-asellaqua_nInitm2-asellaqua_fBasal.pdf"
    # )
    # assert os.path.exists(
    #     "./fig/mesocoso10D_posterior2d-asellaqua_fIngest-asellaqua_kDens.pdf"
    # )
    # assert os.path.exists("./fig/mesocoso10D_likelihoods.pdf")
    # assert os.path.exists("./fig/mesocoso10D_redraw.pdf")
    # assert os.path.exists("./fig/mesocoso10D_deviations.pdf")
    # assert os.path.exists("./fig/mesocoso10D_acceptances.pdf")
    # assert os.path.exists("./fig/mesocoso10D_autocorrelations.pdf")
    # assert os.path.exists("./fig/mesocoso10D_predictions-posterior-1.pdf")
    # assert os.path.exists("./fig/mesocoso10D_predictions-posterior-6.pdf")
    # assert os.path.exists("./fig/mesocoso10D_qq.pdf")