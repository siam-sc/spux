#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import os # noqa: F401
import sys

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
#@pytest.mark.skip (reason = "Fails with: AttributeError: 'Error' object has no attribute 'errmdl'")
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["superflex"], indirect=True)
def test_superflex_parallel (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("plot_config")

    #os.system('rm ./lib/*')
    #os.system('cp ./libmars/* ./lib/')
    #os.system('mv ./libmars/superflex_spux_mars ./lib/superflex_spux')
    os.system('rm ./lib/*')
    os.system('cp ./libeuler/* ./lib/')
    os.system('mv ./lib/superflex_spux_euler ./lib/superflex_spux')

    print("spawning")
    sys.stdout.flush()
    run_example_script ("import script_parallel")

    print("plot_results")
    sys.stdout.flush()
    run_example_script ("plot_results")

    if os.path.exists ('./fig'):
        assert os.path.exists ('./fig/superflex_acceptances.pdf')
        assert os.path.exists ('./fig/superflex_autocorrelations.pdf')
        assert os.path.exists ('./fig/superflex_accuracies.pdf')
        assert os.path.exists ('./fig/superflex_likelihoods.pdf')
        assert os.path.exists ('./fig/superflex_parameters.pdf')
        assert os.path.exists ('./fig/superflex_posteriors2d.pdf')
        assert os.path.exists ('./fig/superflex_posteriors.pdf')
        assert os.path.exists ('./fig/superflex_predictions-posterior-dataset-28.pdf')
        assert os.path.exists ('./fig/superflex_unsuccessfuls.pdf')
    else:
        print ("Skipping plot_results assert as this is likely a bug of pytest")
        sys.stdout.flush()

    os.system('rm ./lib/*')
    os.system('cp ./libmars/* ./lib/')
    os.system('mv ./lib/superflex_spux_mars ./lib/superflex_spux')