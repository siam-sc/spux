#! /usr/bin/env python
# encoding: utf-8

import pytest # noqa: F401
import os # noqa: F401

from examples_utils import run_example_script

# Note: only tests with 'Spawn' connector can be executed here.
# For Split () connector tests, use Euler cluster tests in 'spux/tests_euler'.

@pytest.mark.skip (reason = "mutiple example tests seem not to work properly")
@pytest.mark.integration
@pytest.mark.mpi
@pytest.mark.parametrize("example_setup_and_teardown", ["straightwalk"], indirect=True)
def test_straightwalk_parallel (example_setup_and_teardown):

    path = example_setup_and_teardown  # noqa: F841

    run_example_script ("plot_config")
    # assert os.path.exists ('./fig/straightwalk_distributions-prior.png')
    # assert os.path.exists ('./fig/straightwalk_datasets-position.png')
    # assert os.path.exists ('./fig/straightwalk_errors-dataset-0.png')
    # assert os.path.exists ('./fig/straightwalk_errors-dataset-1.png')
    # assert os.path.exists ('./fig/straightwalk_errors-dataset-2.png')
    # assert os.path.exists ('./fig/straightwalk_distributions-error.png')

    run_example_script ("script_parallel")
    # assert os.path.exists ('./report/straightwalk_environment.txt')
    # assert os.path.exists ('./report/straightwalk_environment.dat')
    # assert os.path.exists ('./report/straightwalk_config.txt')
    # assert os.path.exists ('./report/straightwalk_config.dat')
    # assert os.path.exists ('./report/straightwalk_resources.txt')
    # assert os.path.exists ('./report/straightwalk_resources.dat')

    run_example_script ("plot_results")
    # assert os.path.exists ('./report/straightwalk_MAP.txt')
    # assert os.path.exists ('./report/straightwalk_MAP.dat')
    # assert os.path.exists ('./report/straightwalk_ESS.dat')
    # assert os.path.exists ('./report/straightwalk_ESS.txt')
    # assert os.path.exists ('./fig/straightwalk_parameters.png')
    # assert os.path.exists ('./fig/straightwalk_posteriors.png')
    # assert os.path.exists ('./fig/straightwalk_posteriors2d.png')
    # assert os.path.exists ('./fig/straightwalk_likelihoods.png')
    # assert os.path.exists ('./fig/straightwalk_acceptances.png')
    # assert os.path.exists ('./fig/straightwalk_autocorrelations.png')
    # assert os.path.exists ('./fig/straightwalk_predictions-posterior-dataset-0.png')
    # assert os.path.exists ('./fig/straightwalk_predictions-posterior-dataset-1.png')
    # assert os.path.exists ('./fig/straightwalk_predictions-posterior-dataset-2.png')
    # assert os.path.exists ('./fig/straightwalk_qq.png')