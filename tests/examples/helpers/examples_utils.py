import sys


def run_example_script(script_name):
    '''
    Run script from an example folder.

    Relying on no `if __name__ == 'main'` structure in the example scripts, import the
    script and cleanup afterwards.
    '''
    script_name = script_name.rstrip(".py")
    m = __import__(script_name)
    del m
    del sys.modules[script_name]
    # alt, use `os.system(script_name)`
