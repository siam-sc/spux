import pytest
import os
import shutil
import sys


sys.path.append(os.path.join(os.path.dirname(__file__), 'helpers'))


@pytest.fixture(scope="function")
def example_setup_and_teardown(request):

    example_name = request.param

    # Setup
    print("## Setup")
    path = os.path.normpath (os.path.join(os.path.dirname(__file__), "..", "..", "examples", example_name))
    rootdir = os.getcwd()
    print("Root:", rootdir)
    print("Path:", path)
    os.chdir(path)
    sys.path.insert(0, ".")

    yield path

    # Teardown
    print("## Teardown")
    dirs = ['fig', 'report', 'output']
    for directory in dirs:
        try:
            shutil.rmtree("./%s/" % directory)
            print("Removed ./%s/ sub-directory" % directory)
        except FileNotFoundError:
            print("No ./%s/ sub-directory found" % directory)
    os.chdir(rootdir)
    del sys.path[0]
    print("Cwd:", os.getcwd())
