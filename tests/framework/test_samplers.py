#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pandas
import pytest

from spux.models.randomwalk import Randomwalk
from spux.likelihoods.pf import PF
from spux.samplers.emcee import EMCEE
from spux.utils.seed import Seed

from test_error import error
from test_prior import prior

def approxeq_or_none (x, x_ref, reltol=1e-6, abstol=1e-12):
    if x_ref is None:
        return x is None
    return abs(x - x_ref) < reltol * abs(x_ref) + abstol

def emcee_sampler (chains):

    # === construct dataset
    dataset = pandas.DataFrame (columns=["position"])
    dataset.loc[10] = [16]
    dataset.loc[20] = [22]
    dataset.loc[30] = [28]
    dataset.loc[40] = [34]

    # construct Random Walk model object
    model = Randomwalk ()

    # construct likelihood object (here Particle Filtering)
    likelihood = PF ()
    likelihood.assign (model, error, dataset)

    # construct sampler
    sampler = EMCEE (chains)
    sampler.assign (likelihood, prior)

    return sampler

@pytest.mark.parametrize (
    "seed, chains, acceptance, samples, likelihoods",
    [(2, 5, None, {"origin": 10 * [12.0], "drift": 10 * [0.4]}, 10 * [None])],
    )
def test_emcee (seed, chains, acceptance, samples, likelihoods):

    samples = pandas.DataFrame.from_dict (samples)
    sampler = emcee_sampler (chains)
    sampler.executor.init ()
    sampler.setup (seed=Seed(seed))
    sampler.init ()
    samples_out, infos_out = sampler (2 * chains, checkpointer=None)
    sampler.executor.exit ()

    #assert approxeq_or_none (sampler.acceptance, acceptance)
    assert len (samples_out) == 2 * chains

    return
    for i_row in range(len(samples_out)):
        s_out = samples_out.loc[i_row]
        s = samples.loc[i_row]
        assert approxeq_or_none(s_out.init, s.init), (
            "Sample init mismatch in row %d" % i_row
        )
        assert approxeq_or_none(s_out.bias, s.bias), (
            "Sample bias mismatch in row %d" % i_row
        )
    #assert all (approxeq_or_none(l_out, l) for l_out, l in zip(likelihoods_out, likelihoods))
