#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pytest # noqa: F401

from scipy import stats

from spux.distributions.tensor import Tensor

distributions = {}

distributions ['origin'] = stats.uniform (loc=-1, scale=2)
distributions ['drift'] = stats.uniform (loc=0, scale=1)
#distributions [r'\sigma'] = stats.lognorm (scale=numpy.exp(3), s=1)

# construct a joint distribution for a vector of independent parameters by tensorization
prior = Tensor (distributions)

def test_prior ():

    assert prior