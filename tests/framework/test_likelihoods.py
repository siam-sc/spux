#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pandas
import pytest

from spux.likelihoods.ensemble import Ensemble
from spux.executors.serial import Serial
from spux.likelihoods.pf import PF
from spux.models.randomwalk import Randomwalk
from spux.utils.seed import Seed

from test_error import error

@pytest.fixture
def parameters():
    # set parameters
    return {"origin": 10, "drift": 0.6}

# @pytest.fixture
# def ensemble_workers():
#     # determine number of available workers
#     return MPI.COMM_WORLD.Get_size()

@pytest.mark.parametrize(
    "seed, sample, resample",
    [(1, {0: 64, 1: 78}, {0: 122, 1: 122}), (128, {0: 60, 1: 74}, {0: 108, 1: 114})],
)
def test_ensemble(seed, sample, resample, parameters, particles=2):

    # executor
    executor = Serial ()

    # construct Random Walk model object
    model = Randomwalk ()

    # executor setup and init
    executor.setup (task=model)
    executor.init ()

    # construct ensemble
    inputset = None
    ensemble = Ensemble (model, inputset, parameters, error)

    # setup ensemble
    ensemble.setup (verbosity=0, seed=Seed(seed))

    # get indices ensembles
    indices = list (range (particles))

    # initialize ensemble in executor
    executor.connect (ensemble, indices)

    # run all particles in ensemble up to specified time
    predictions = executor.call ('run', args = [100])

    # observe all particles
    predictions = { index : prediction ['position'] for index, prediction in predictions.items () }
    assert predictions == sample

    # construct new indices
    indices = [0] * particles

    # advance
    executor.call ('advance')

    # resample
    executor.resample (indices)

    # run all particles in ensemble up to specified time
    predictions = executor.call ('run', args = [200])

    # observe all particles
    predictions = { index : prediction ['position'] for index, prediction in predictions.items () }
    assert predictions == resample

    # exit executor
    executor.exit ()

def pf_likelihood (**pf_kwargs):

    model = Randomwalk ()

    # === construct dataset
    dataset = pandas.DataFrame (columns=["position"])
    dataset.loc [100] = [70]
    dataset.loc [200] = [130]

    # construct likelihood object
    likelihood = PF (**pf_kwargs)
    likelihood.assign (model, error, dataset)

    return likelihood

@pytest.mark.parametrize(
    "seed, par_origin, par_trend, particles, loglikelihood_value",
    [
        (1, 10, 0.6, 2, -52.53102341544092),
        (128, 10, 0.6, 2, -13.2241714275),
        (64, 5, 0.8, 10, -410.74990007183749),
    ],
)
def test_pf(seed, par_origin, par_trend, particles, loglikelihood_value):

    parameters = {"origin": par_origin, "drift": par_trend}
    likelihood = pf_likelihood (particles=particles)
    likelihood.executor.init ()
    likelihood.setup (seed=Seed(seed))

    # evaluate likelihood
    L, _ = likelihood (parameters)
    assert abs(L - loglikelihood_value) < 1e-8

    # re-evaluate likelihood w/ params yielding ca. 0
    L, _ = likelihood ({"origin": 20, "drift": 0.2})
    assert L < -30

    # init executor
    likelihood.executor.exit ()

    # try:
    #     # evaluate likelihood
    #     L, _ = likelihood.evaluate (parameters, particles=particles)
    #     assert abs(numpy.log10(L) - loglikelihood_value) < 1e-8
    #     # re-evaluate likelihood w/ params yielding ca. 0
    #     L, _ = likelihood.evaluate({"init": 20, "bias": 0.6}, particles=20)
    #     assert abs(L) < 1e-16
    # finally:
    #     # in case of multiple runs, give MPI a bit of time
    #     time.sleep(0.5)
