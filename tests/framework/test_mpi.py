#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pytest # noqa: F401

@pytest.mark.mpi
def test_mpi_and_not_integration ():

    # a dummy test to keep pytest happy for this currently empty test group
    pass