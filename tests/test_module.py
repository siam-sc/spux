#!/usr/bin/env python
# -*- coding: utf-8 -*-

def test_imports():
    from spux import executors # noqa: F401
    from spux import drivers # noqa: F401
    from spux import io # noqa: F401
    from spux import likelihoods # noqa: F401
    from spux import distributions # noqa: F401
    from spux import models # noqa: F401
    from spux import processes # noqa: F401
    from spux import samplers # noqa: F401
    from spux import plot # noqa: F401
    from spux import report # noqa: F401
    from spux import utils # noqa: F401
