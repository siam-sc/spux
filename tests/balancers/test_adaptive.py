#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pytest

from spux.executors.balancers.adaptive import Adaptive

_ensemble_indices = list(range(7))
_ensembles_dict = {2: [[0, 1, 2, 3], [4, 5, 6]], 4: [[0, 1], [2, 3], [4, 5], [6]]}
_routings_indices = [2, 2, 2, 2, 2, 1, 6]
_ensembles_routings_pair_dict = {  # pairs of expected output ensembles and routings
    (Adaptive, 2): (
        [[0, 1, 2, 3], [6, 4, 5]],
        [
            [
                (1, 0, 0, 0),
                (2, 0, 0, 1),
                (2, 0, 0, 2),
                (2, 0, 0, 3),
                (2, 0, 1, 4),
                (2, 0, 1, 5),
            ],
            [(6, 1, 1, 6), (2, 0, 1, 4), (2, 0, 1, 5)],
        ],
    ),
    (Adaptive, 4): (
        [[0, 5], [1, 2], [3, 4], [6]],
        [
            [(1, 0, 0, 0), (2, 1, 0, 5)],
            [(2, 1, 1, 1), (2, 1, 1, 2), (2, 1, 2, 3), (2, 1, 2, 4), (2, 1, 0, 5)],
            [(2, 1, 2, 3), (2, 1, 2, 4)],
            [(6, 3, 3, 6)],
        ],
    ),
}


def _test_params(balancer_cls, n_workers):
    return (
        balancer_cls,
        n_workers,
        _ensemble_indices,
        _ensembles_dict[n_workers],
        _routings_indices,
        _ensembles_routings_pair_dict[(balancer_cls, n_workers)],
    )


@pytest.mark.parametrize(
    "balancer_cls, n_workers, indices, ensembles, routings_indices, "
    "ensembles_routings_pair",
    [
        _test_params(Adaptive, 2),
        _test_params(Adaptive, 4),
    ],
)
def test_balancer(
    balancer_cls,
    n_workers,
    indices,
    ensembles,
    routings_indices,
    ensembles_routings_pair,
):
    balancer = balancer_cls ()
    for a, b in zip (ensembles, balancer.ensembles (indices, n_workers)):
        assert all (a == b)
    ensembles_routings_pair_out = balancer.routings (ensembles, routings_indices)
    assert ensembles_routings_pair == ensembles_routings_pair_out
