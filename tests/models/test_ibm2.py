import os
import sys
import numpy
import pytest # noqa: F401
import shutil
from spux.models.ibm import IBM

def test_ibm ():

    path = os.path.join (os.path.dirname (__file__), '../../examples/IBM_2species')
    rootdir = os.getcwd ()
    print ('Root:', rootdir)
    print ('Path:', path)
    os.chdir (path)
    sys.path.insert (0, '.')

    classpath = 'IBM-Bugs_OpenJDK.jar'
    config = 'generalSettings_extFiltering.txt'
    inputpath = 'input'
    paramtypefl = "parameters.types"
    from initial import initial

    # prepare running dir (aka sandbox) for ibm1
    from spux.utils import sandbox
    sandbox1 = sandbox.Sandbox (path = "testibm/m1", target = None, template = inputpath)

    # prepare seed
    from spux.utils import seed
    seed1 = seed.Seed (4, name='sampler')

    # construct IBM model object
    ibm1 = IBM(config, classpath, paramtypefl=paramtypefl, initial=initial)

    # isolate IBM model to sandbox
    ibm1.isolate(sandbox=sandbox1,verbosity=1)

    # plant IBM model
    ibm1.plant(seed=seed1)

    # set parameters and init
    parameters = {}
    parameters["prey_kDens"] = 26
    parameters["predator_kDens"] = 16
    inputset = None
    ibm1.init(inputset, parameters)

    # run IBM model up to time = 1000
    ibm1.run(time=1000)

    # report observations
    obs1000 = numpy.array(ibm1.model.observe())

    #save state
    state = ibm1.save()

    # run IBM model up to time = 2500
    seed2 = seed.Seed (8, name='sampler')
    ibm1.plant(seed=seed2)
    ibm1.run(time=2500)

    # report observations
    obs2500 = numpy.array(ibm1.model.observe())

    # create new IBM model object
    ibm2 = IBM(config, classpath, paramtypefl=paramtypefl, initial=initial)

    # create new run folder and do setup
    sandbox2 = sandbox.Sandbox (path = "testibm/m2", target = None, template = 'input')

    # plan IBM model to sandbox
    ibm2.isolate(sandbox=sandbox2,verbosity=1)

    ibm2.time = 1000

    ibm2.load(state) # state is testibm/m1/state.dat
    ibm2.plant(seed=seed2)

    test = (numpy.array(ibm2.model.observe ()) == obs1000)
    assert( test.all() )

    ibm2.run(time=2500)
    test = (numpy.array(ibm2.model.observe()) == obs2500)
    assert( test.all() )

    shutil.rmtree("testibm")

    os.chdir (rootdir)
    del sys.path [0]

#@pytest.fixture
#def ibm_java():
#    #yield ibm.IBM.get_java_driver(jvmargs="-XX:+UnlockCommercialFeatures")
#    # # yield in with statement ensures that __exit__ will be called as a cleanup
#    # with ibm.IBM.get_java_driver(jvmargs="-XX:+UnlockCommercialFeatures") as j:
#    #     yield j


## FIXME: fails when run w/ other Java-driver based tests; JPype JVM issue:
##        can not load other jar files later?
#@pytest.mark.xfail(reason="Class mesoModel.TheModel not found")
#def test_ibm_jar(ibm_java):
#    if ibm_java is not None:
#        assert (
#            ibm_java.get_class(ibm.IBM.MODEL_CLS)
#            is not None
#        ), "Can't fetch IBM model class"
#        assert (
#            ibm_java.get_class(ibm.IBM.INTERFACE_CLS)
#            is not None
#        ), "Can't fetch IBM interface class"
#
#
#@pytest.mark.xfail(reason="settings for inputpath not working")
#def test_ibm():
#    # FIXME: No such file or directory: 'input'
#    ibm.test()
