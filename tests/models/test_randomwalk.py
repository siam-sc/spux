#! /usr/bin/env python
# encoding: utf-8
from __future__ import absolute_import, division, print_function

import pytest
from spux.models.randomwalk import Randomwalk
from spux.utils.seed import Seed

# Function scope because `run` only runs the walk if step `time` wasn't reached before
@pytest.fixture(scope="function")
def randomwalk (inputset=None, parameters={"origin": 10, "drift": 0.6}):

    # construct randomwalk model object
    model = Randomwalk ()
    # isolate and plant randomwalk model
    model.isolate ()
    model.plant ()
    # initialize randomwalk model with given parameters
    model.init (inputset, parameters)
    return model

@pytest.mark.parametrize("seed, expected", [(4, 16), (128, 18)])
def test_randomalk(seed, expected, randomwalk):
    randomwalk.plant (seed=Seed(seed))
    results = randomwalk.run (time=10)
    assert results ['position'] == expected
