
.. _history:

=======
History
=======

0.4.0 (2019-06-12)
------------------

* Improvements in sandboxing, built-in serialization, report generation, plotting, thinning,
  support for legacy connector, and improvements in inference continuation procedure.

0.3.0 (2019-04-10)
------------------

* Many leaps forward: improvements in applications, local sandboxes, plotting, and many more.

0.2.1 (2019-03-06)
------------------

* Initial release for the spux project kickoff.
