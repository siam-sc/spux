numba
pip>=9.0.1
Sphinx>=1.7.1
setuptools
wheel
twine>=1.10.0
